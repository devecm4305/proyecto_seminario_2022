package com.inerv.gt.sidq.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.inerv.gt.sidq.entity.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Integer> {
	
	Optional<Cliente> findByNit(String nit);
	Optional<Cliente> findByCodigo(String codigo);

	@Query("SELECT c FROM Cliente c WHERE UPPER(c.primerNombre) =:primerNombre and UPPER(c.primerApellido) =:primerApellido")
	List<Cliente> findByNombreApellido(@Param("primerNombre")String primerNombre, @Param("primerApellido")String primerApellido);

}
