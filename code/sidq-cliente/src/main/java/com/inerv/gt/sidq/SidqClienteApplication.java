package com.inerv.gt.sidq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SidqClienteApplication {

	public static void main(String[] args) {
		SpringApplication.run(SidqClienteApplication.class, args);
	}

}
