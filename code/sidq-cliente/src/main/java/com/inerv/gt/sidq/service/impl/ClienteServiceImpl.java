package com.inerv.gt.sidq.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.inerv.gt.sidq.entity.Cliente;
import com.inerv.gt.sidq.repository.ClienteRepository;
import com.inerv.gt.sidq.service.ClienteService;
import com.inerv.gt.sidq.service.SidQServiceGenericImpl;

/**
 * 
 * @author ervin
 *
 */
@Service
public class ClienteServiceImpl extends SidQServiceGenericImpl<Cliente, ClienteRepository> implements ClienteService{

	@Override
	public Optional<Cliente> findByNit(String nit) {
		return repository.findByNit(nit);
	}

	@Override
	public Optional<Cliente> findByCodigo(String codigo) {
		return repository.findByCodigo(codigo);
	}

	@Override
	public List<Cliente> findByNombreApellido(String primerNombre, String primerApellido) {
		return repository.findByNombreApellido(primerNombre, primerApellido);
	}
	
}
