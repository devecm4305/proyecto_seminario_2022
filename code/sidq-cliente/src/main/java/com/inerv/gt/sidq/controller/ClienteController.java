package com.inerv.gt.sidq.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inerv.gt.sidq.entity.Cliente;
import com.inerv.gt.sidq.service.ClienteService;

/**
 * 
 * @author ervin
 *
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/v1.0/clientes")
public class ClienteController extends SidQControllerGeneric<Cliente, ClienteService>{
	
	@GetMapping(path = "/findByNit")
	ResponseEntity<Cliente> nit(@RequestParam("nit")String nit){
		Optional<Cliente> clienteData = service.findByNit(nit);
		if(clienteData.isPresent()) {
			return new ResponseEntity<>(clienteData.get(),HttpStatus.OK); 
		}else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	
	@GetMapping(path = "/findByCodigo")
	ResponseEntity<Cliente> codigo(@RequestParam("codigo")String codigo){
		Optional<Cliente> clienteData = service.findByCodigo(codigo);
		if(clienteData.isPresent()) {
			return new ResponseEntity<>(clienteData.get(),HttpStatus.OK); 
		}else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(path = "/findByNombreApellido")
	ResponseEntity<List<Cliente>> findNombreApellido(@RequestParam(name = "primerNombre", required = true)String primerNombre, @RequestParam(name = "primerApellido", required = true)String primerApellido){
		List<Cliente> clientesList = service.findByNombreApellido(primerNombre.toUpperCase(), primerApellido.toUpperCase());
		return new ResponseEntity<>(clientesList, HttpStatus.OK);
	}

}
