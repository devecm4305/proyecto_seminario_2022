package com.inerv.gt.sidq.service;

import java.util.List;
import java.util.Optional;

import com.inerv.gt.sidq.entity.Cliente;

/**
 * 
 * @author ervin
 *
 */
public interface ClienteService extends SidQServiceGeneric<Cliente> {
	
	Optional<Cliente> findByNit(String nit);
	Optional<Cliente> findByCodigo(String codigo);
	List<Cliente> findByNombreApellido(String primerNombre, String primerApellido);

}
