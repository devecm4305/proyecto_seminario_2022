package com.inerv.gt.sidq.entity;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ervin
 *
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name="compra")
public class Compra implements Serializable {

	private static final long serialVersionUID = 4817422533350622354L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "codigo", nullable = false)
	private String codigo;
	
	@Column(name = "fecha_creacion")
	private Date fechaCreacion;
	
	@Column(precision = 8, scale = 2, nullable = false)
	private Double total;
	
	@Column(name = "forma_pago", nullable = false)
	private Integer formaPago;
	
	private Integer usuario;
	
	@PrePersist
	void prePersist() {
		fechaCreacion = new Date();
	}
	
}
