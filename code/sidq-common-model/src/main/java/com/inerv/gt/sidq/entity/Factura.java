package com.inerv.gt.sidq.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ervin
 *
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name="factura")
public class Factura implements Serializable{

	private static final long serialVersionUID = -7857849172455114285L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "fecha_creacion")
	private LocalDateTime fechaCreacion;
	
	private String autorizacion;
	
	@Column(name = "fecha_emision")
	private Date fechaEmision;
	
	@Column(name = "fecha_certificacion")
	private Date fechaCertificacion;
	
	private String serie;
	
	private Integer caja;
	
	@Column(name = "forma_pago")
	private Integer formaPago;
	
	private Integer usuario;
	
	private Integer Cliente;
	
	private Integer estado;
	
	@Column(precision = 6, scale = 2, nullable = false)
	private Double monto;
	
	@PrePersist
	void prePersist() {
		fechaCreacion = LocalDateTime.now();
		fechaEmision = new Date();
		fechaCertificacion = new Date();
	}
	
}
