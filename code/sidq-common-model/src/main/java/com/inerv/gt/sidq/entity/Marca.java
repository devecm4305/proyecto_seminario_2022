package com.inerv.gt.sidq.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author ecanuz
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name="marca", schema="sidq")
public class Marca implements Serializable{
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

    private String nombre;

    private Integer estado;

    @Column(name = "fecha_creacion")
    private LocalDateTime fechaCreacion;
    
    private String descripcion;

    private Integer calificacion;

    @PrePersist
    void prePersist() {
        fechaCreacion = LocalDateTime.now();
    }

}
