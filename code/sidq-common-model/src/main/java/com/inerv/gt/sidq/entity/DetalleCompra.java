package com.inerv.gt.sidq.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ervin
 *
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name="detalle_compra")
public class DetalleCompra implements Serializable{

	private static final long serialVersionUID = 3393613576009588702L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private Integer compra;
	
	private Integer producto;
	
	private Integer cantidad;
	
	@Column(name = "precio_unitario",precision = 8, scale = 2, nullable = false)
	private Double precioUnitario;
	
	@Column(name = "sub_total",precision = 8, scale = 2, nullable = false)
	private Double subTotal;
	
}
