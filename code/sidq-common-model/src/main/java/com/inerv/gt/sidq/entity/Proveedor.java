package com.inerv.gt.sidq.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author ervin
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name="proveedor", schema="sidq")
public class Proveedor implements Serializable{
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -1L;

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

    private String nombre;

    private String direccion;

    @Column(name = "telefono_contacto")
    private String telefonoContacto;

    @Column(name = "celular_contacto")
    private String celularContacto;

    @Column(name = "email_contacto")
    private String emailContacto;

    private String pagina;

    private String codigo;

    private Integer estado;

}
