package com.inerv.gt.sidq.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ervin
 *
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name="detalle_factura")
public class DetalleFactura {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(precision = 4, scale = 2, nullable = false)
	private Double descuento;
	
	@Column(nullable = false)
	private Integer cantidad;
	
	@Column(nullable = false)
	private Integer factura;
	
	@Column(nullable = false)
	private Integer producto;
	
	private String descripcion;
	
	@Column(name = "sub_total",precision = 4, scale = 2, nullable = false)
	private Double subTotal;
	
}
