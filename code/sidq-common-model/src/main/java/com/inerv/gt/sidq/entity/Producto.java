package com.inerv.gt.sidq.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name="producto", schema="sidq")
public class Producto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

    private String nombre;

    private String descripcion;

    private Integer estado;

    private Integer existencias;

    @Column(name = "precio_unitario", precision = 8, scale = 2, nullable = false)
    private Double precioUnitario;

    private Integer categoria;

    private Integer marca;

    private Integer proveedor;
    
    @Column(name = "codigo_producto")
    private String codigoProducto;
    
}
