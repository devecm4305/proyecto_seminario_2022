package com.inerv.gt.sidq.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inerv.gt.sidq.entity.Producto;
import com.inerv.gt.sidq.service.ProductoService;

/**
 * @author ecanuz
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/v1.0/productos")
public class ProductoController extends SidQControllerGeneric<Producto, ProductoService>{
	
	@GetMapping(path="/getExistencias")
	ResponseEntity<List<Producto>> getExistencias(){
		List<Producto> productosExistencias = service.findProductosOrderByExistenciasDesc();
		return new ResponseEntity<>(productosExistencias, HttpStatus.OK);
	}
    
	@GetMapping(path = "/findByProveedor")
	ResponseEntity<List<Producto>> proveedor(@RequestParam("proveedor")Integer proveedor){
		List<Producto> proveedorData = service.findByProveedor(proveedor);
		return new ResponseEntity<>(proveedorData, HttpStatus.OK);
	}
	
	@GetMapping(path = "/findByMarca")
	ResponseEntity<List<Producto>> marca(@RequestParam("marca")Integer marca){
		List<Producto> marcaData = service.findByMarca(marca);
		return new ResponseEntity<>(marcaData, HttpStatus.OK);
	}
	
	@GetMapping(path = "/findByNombre")
	ResponseEntity<List<Producto>> findByNombre(@RequestParam("nombre")String nombre){
		List<Producto> marcaData = service.findByNombreProducto(nombre);
		return new ResponseEntity<>(marcaData, HttpStatus.OK);
	}
	
	@GetMapping(path = "/findByProveedorAndMarca")
	ResponseEntity<List<Producto>> proveedorMarca(@RequestParam("proveedor")Integer proveedor, @RequestParam("marca")Integer marca){
		List<Producto> marcaData = service.findByProductoMarca(proveedor, marca);
		return new ResponseEntity<>(marcaData, HttpStatus.OK);
	}
	
	@GetMapping(path = "/findByCodigoProducto")
	ResponseEntity<Producto> codigoProducto(@RequestParam("codigoProducto")String codigoProducto){
		Producto productoData = service.findByCodigoProducto(codigoProducto);
		return new ResponseEntity<>(productoData, HttpStatus.OK);
	}
	
}
