package com.inerv.gt.sidq.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.inerv.gt.sidq.entity.Producto;
import com.inerv.gt.sidq.repository.ProductoRepository;
import com.inerv.gt.sidq.service.ProductoService;
import com.inerv.gt.sidq.service.SidQServiceGenericImpl;

@Service
public class ProductoServiceImpl extends SidQServiceGenericImpl<Producto, ProductoRepository> implements ProductoService{

	@Override
	public List<Producto> findByProveedor(Integer proveedor) {
		return repository.findByProveedor(proveedor);
	}

	@Override
	public List<Producto> findByMarca(Integer marca) {
		return repository.findByMarca(marca);
	}

	@Override
	public List<Producto> findByProductoMarca(Integer proveedor, Integer marca) {
		return repository.findByProductoMarca(proveedor, marca);
	}

	@Override
	public Producto findByCodigoProducto(String codigoProducto) {
		return repository.findByCodidoProducto(codigoProducto);
	}

	@Override
	public List<Producto> findByNombreProducto(String nombreProducto) {
		return repository.findByNombre(nombreProducto);
	}

	@Override
	public List<Producto> findProductosOrderByExistenciasDesc() {
		return repository.findProductosOrderByExistenciasDesc();
	}
    
}
