package com.inerv.gt.sidq.service;

import java.util.List;

import com.inerv.gt.sidq.entity.Producto;

public interface ProductoService extends SidQServiceGeneric<Producto>{
    
	List<Producto> findByProveedor(Integer proveedor);
	List<Producto> findByMarca(Integer marca);
	
	List<Producto> findByProductoMarca(Integer proveedor, Integer marca);
	
	Producto findByCodigoProducto(String codigoProducto);
	
	List<Producto> findByNombreProducto(String nombreProducto);
	
	List<Producto> findProductosOrderByExistenciasDesc();
	
}
