package com.inerv.gt.sidq.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.inerv.gt.sidq.entity.Producto;

/**
 * 
 */
public interface ProductoRepository extends JpaRepository<Producto, Integer>{
	
	@Query("SELECT c FROM Producto c ORDER BY c.existencias DESC")
	List<Producto> findProductosOrderByExistenciasDesc();
	
	List<Producto> findByProveedor(Integer proveedor);
	List<Producto> findByMarca(Integer marca);
	
	@Query("SELECT c FROM Producto c WHERE c.proveedor =:proveedor AND c.marca =:marca")
	List<Producto> findByProductoMarca(@Param("proveedor")Integer proveedor, @Param("marca")Integer marca);
	
	@Query("SELECT c FROM Producto c WHERE c.codigoProducto =:codigoProducto")
	Producto findByCodidoProducto(@Param("codigoProducto")String codigoProducto); 
	
	@Query("SELECT c FROM Producto c WHERE c.nombre =:nombreProducto")
	List<Producto> findByNombre(@Param("nombreProducto")String nombreProducto);
	
    
}
