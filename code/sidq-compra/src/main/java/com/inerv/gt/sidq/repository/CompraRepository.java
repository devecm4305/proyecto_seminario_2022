package com.inerv.gt.sidq.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inerv.gt.sidq.entity.Compra;

public interface CompraRepository extends JpaRepository<Compra, Integer> {

}
