package com.inerv.gt.sidq.service;

import com.inerv.gt.sidq.entity.Compra;

/**
 * 
 * @author ervin
 *
 */
public interface CompraService extends SidQServiceGeneric<Compra> {

}
