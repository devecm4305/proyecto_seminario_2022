package com.inerv.gt.sidq.service.impl;

import org.springframework.stereotype.Service;

import com.inerv.gt.sidq.entity.Compra;
import com.inerv.gt.sidq.repository.CompraRepository;
import com.inerv.gt.sidq.service.CompraService;
import com.inerv.gt.sidq.service.SidQServiceGenericImpl;

/**
 * 
 * @author ervin
 *
 */
@Service
public class CompraServiceImpl extends SidQServiceGenericImpl<Compra, CompraRepository> implements CompraService{

}
