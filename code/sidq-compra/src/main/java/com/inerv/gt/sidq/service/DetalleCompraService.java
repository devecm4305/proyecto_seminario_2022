package com.inerv.gt.sidq.service;

import java.util.List;

import com.inerv.gt.sidq.entity.DetalleCompra;

/**
 * 
 * @author ervin
 *
 */
public interface DetalleCompraService extends SidQServiceGeneric<DetalleCompra> {

	List<DetalleCompra> findByCompra(Integer compraId);
	
	
}
