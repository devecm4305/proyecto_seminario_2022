package com.inerv.gt.sidq.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inerv.gt.sidq.entity.DetalleCompra;
import com.inerv.gt.sidq.service.DetalleCompraService;

/**
 * 
 * @author ervin
 *
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/v1.0/detalleCompras")
public class DetalleCompraController extends SidQControllerGeneric<DetalleCompra, DetalleCompraService>{
	
	@GetMapping(path = "/findByIdCompra")
	ResponseEntity<List<DetalleCompra>> productos(@RequestParam("idCompra")Integer idCompra){
		List<DetalleCompra> detalleCompraData = service.findByCompra(idCompra);
		return new ResponseEntity<>(detalleCompraData, HttpStatus.OK);
	}

}
