package com.inerv.gt.sidq.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inerv.gt.sidq.entity.DetalleCompra;

public interface DetalleCompraRepository extends JpaRepository<DetalleCompra, Integer> {

	List<DetalleCompra> findByCompra(Integer compra);
	
}
