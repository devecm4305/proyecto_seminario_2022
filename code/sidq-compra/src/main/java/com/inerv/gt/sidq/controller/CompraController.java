package com.inerv.gt.sidq.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inerv.gt.sidq.entity.Compra;
import com.inerv.gt.sidq.service.CompraService;

/**
 * 
 * @author ervin
 *
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/v1.0/compras")
public class CompraController extends SidQControllerGeneric<Compra, CompraService>{

}
