package com.inerv.gt.sidq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SidqCompraApplication {

	public static void main(String[] args) {
		SpringApplication.run(SidqCompraApplication.class, args);
	}

}
