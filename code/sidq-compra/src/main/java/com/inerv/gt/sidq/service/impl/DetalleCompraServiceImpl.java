package com.inerv.gt.sidq.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.inerv.gt.sidq.entity.DetalleCompra;
import com.inerv.gt.sidq.repository.DetalleCompraRepository;
import com.inerv.gt.sidq.service.DetalleCompraService;
import com.inerv.gt.sidq.service.SidQServiceGenericImpl;

@Service
public class DetalleCompraServiceImpl extends SidQServiceGenericImpl<DetalleCompra, DetalleCompraRepository> implements DetalleCompraService {

	@Override
	public List<DetalleCompra> findByCompra(Integer compraId) {
		return repository.findByCompra(compraId);
	}

}
