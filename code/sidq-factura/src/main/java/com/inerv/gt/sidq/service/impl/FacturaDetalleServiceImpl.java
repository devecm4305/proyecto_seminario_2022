package com.inerv.gt.sidq.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.inerv.gt.sidq.entity.DetalleFactura;
import com.inerv.gt.sidq.repository.FacturaDetalleRepository;
import com.inerv.gt.sidq.service.FacturaDetalleService;
import com.inerv.gt.sidq.service.SidQServiceGenericImpl;

/**
 * 
 * @author ervin
 *
 */
@Service
public class FacturaDetalleServiceImpl extends SidQServiceGenericImpl<DetalleFactura, FacturaDetalleRepository> implements FacturaDetalleService{

	@Override
	public List<DetalleFactura> findByFactura(Integer factura) {
		return repository.findByFactura(factura);
	}

	@Override
	public List<Object[]> findProductoByMes(String fechaInicial, String fechaFinal) {
		return repository.findProductoByMes(fechaInicial, fechaFinal);
	}
	
}
