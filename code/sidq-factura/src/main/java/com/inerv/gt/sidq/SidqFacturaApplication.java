package com.inerv.gt.sidq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SidqFacturaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SidqFacturaApplication.class, args);
	}

}
