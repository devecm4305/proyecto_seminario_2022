package com.inerv.gt.sidq.service;

import java.util.List;

import com.inerv.gt.sidq.entity.DetalleFactura;

/**
 * 
 * @author ervin
 *
 */
public interface FacturaDetalleService extends SidQServiceGeneric<DetalleFactura> {
	
	List<DetalleFactura> findByFactura(Integer factura);
	
	List<Object[]> findProductoByMes(String fechaInicial, String fechaFinal);
	
}
