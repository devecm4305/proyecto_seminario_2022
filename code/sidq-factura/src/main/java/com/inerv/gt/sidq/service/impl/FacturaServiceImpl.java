package com.inerv.gt.sidq.service.impl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.inerv.gt.sidq.entity.Factura;
import com.inerv.gt.sidq.repository.FacturaRepository;
import com.inerv.gt.sidq.service.FacturaService;
import com.inerv.gt.sidq.service.SidQServiceGenericImpl;

/**
 * 
 * @author ervin
 *
 */
@Service
public class FacturaServiceImpl extends SidQServiceGenericImpl<Factura, FacturaRepository> implements FacturaService{

	@Override
	@Transactional
	public void anulaFactura(Integer idFactura) {
		repository.anulaFactura(idFactura);
	}

}
