package com.inerv.gt.sidq.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inerv.gt.sidq.entity.Factura;
import com.inerv.gt.sidq.service.FacturaService;

/**
 * 
 * @author ervin
 *
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/v1.0/facturas")
public class FacturaController extends SidQControllerGeneric<Factura, FacturaService>{

	@DeleteMapping(path="/anular")
	ResponseEntity<String> anularFactura(@RequestParam("idFactura")Integer idFactura){
		service.anulaFactura(idFactura);
		return new ResponseEntity<>("",HttpStatus.ACCEPTED);
	}
}
