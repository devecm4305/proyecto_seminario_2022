package com.inerv.gt.sidq.controller;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inerv.gt.sidq.entity.DetalleFactura;
import com.inerv.gt.sidq.response.dto.DetalleProductoMes;
import com.inerv.gt.sidq.service.FacturaDetalleService;

/**
 * 
 * @author ervin
 *
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/v1.0/facturasDetalles")
public class FacturaDetalleController extends SidQControllerGeneric<DetalleFactura, FacturaDetalleService> {
	
	private static final Logger LOG = LoggerFactory.getLogger(FacturaDetalleController.class);

	@GetMapping(path = "/findByFactura")
	ResponseEntity<List<DetalleFactura>> detalleByFactura(@RequestParam("factura")Integer factura){
		List<DetalleFactura> detalleData = service.findByFactura(factura);
		return new ResponseEntity<>(detalleData, HttpStatus.OK);
	}
	
	@GetMapping(path = "/findProductosByMes")
	ResponseEntity<List<DetalleProductoMes>> findProductosByMes(){
		
		LocalDate currentDate = LocalDate.now();
        currentDate.getMonth();
        LocalDate lastDayOfMonthDate  = currentDate.withDayOfMonth(
                                        currentDate.getMonth().length(currentDate.isLeapYear()));
		LOG.info("Last date of the month: "+lastDayOfMonthDate);
        
		LocalDate firstDateOfMonth = currentDate.with(TemporalAdjusters.firstDayOfMonth());
		
		LOG.info("First date of the month: "+firstDateOfMonth);
		
		List<DetalleProductoMes> list = new ArrayList<>();
		List<Object[]> details = service.findProductoByMes(firstDateOfMonth.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")),lastDayOfMonthDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
		details.forEach(item ->{
			list.add(DetalleProductoMes.builder().nombre((String) item[0]).cantidad((BigInteger) item[1]).fechaCreacion(new SimpleDateFormat("yyyy-MM-dd").format(item[2])).build());
		});
		return new ResponseEntity<>(list, HttpStatus.OK);
	}
	
}
