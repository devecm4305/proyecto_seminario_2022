package com.inerv.gt.sidq.response.dto;

import java.math.BigInteger;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ervin
 *
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DetalleProductoMes {

	private String nombre;
	
	private BigInteger cantidad;
	
	private String fechaCreacion;
	
}
