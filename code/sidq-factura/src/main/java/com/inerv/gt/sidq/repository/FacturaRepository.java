package com.inerv.gt.sidq.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.inerv.gt.sidq.entity.Factura;

/**
 * 
 * @author ervin
 *
 */
public interface FacturaRepository extends JpaRepository<Factura, Integer> {

	@Modifying
	@Query("update Factura f set f.estado = 0 where f.id= :idFactura")
	void anulaFactura(@Param("idFactura")Integer idFactura);
	
}
