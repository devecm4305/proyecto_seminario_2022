package com.inerv.gt.sidq.service;

import com.inerv.gt.sidq.entity.Factura;

/**
 * 
 * @author ervin
 *
 */
public interface FacturaService extends SidQServiceGeneric<Factura> {

	void anulaFactura(Integer idFactura);
	
}
