package com.inerv.gt.sidq.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.inerv.gt.sidq.entity.DetalleFactura;

/**
 * 
 * @author ervin
 *
 */
public interface FacturaDetalleRepository extends JpaRepository<DetalleFactura, Integer> {

	List<DetalleFactura> findByFactura(Integer factura);
	
	//@Query(value="select p.nombre, df.cantidad, f.fecha_creacion from sidq.detalle_factura df inner join sidq.factura f on df.factura = f.id inner join sidq.producto p on p.id =df.producto where f.fecha_creacion between '2022-05-01' and '2022-10-31' group by p.nombre, df.cantidad, f.fecha_creacion order by f.fecha_creacion", nativeQuery=true)
	@Query(value="select p.nombre, count(nombre) as cantidad , f.fecha_creacion from sidq.detalle_factura df inner join sidq.factura f on df.factura = f.id inner join sidq.producto p on p.id =df.producto where f.fecha_creacion between '2022-05-01' and '2022-10-31' group by p.nombre, f.fecha_creacion order by f.fecha_creacion", nativeQuery = true)
	List<Object[]> findProductoByMes(String fechaInicial, String fechaFinal);
	
}
