import { KeycloakConfig } from 'keycloak-js';

const keycloakConfig: KeycloakConfig = {
  url: 'http://localhost:7600/auth',
  realm: 'sidq',
  clientId: 'sidq_client'
}

export default keycloakConfig;
