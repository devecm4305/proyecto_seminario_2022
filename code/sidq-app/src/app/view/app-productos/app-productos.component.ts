import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Table } from 'primeng/table';
import { Producto } from 'src/app/model/producto';
import { CategoriaService } from 'src/app/service/categoria.service';
import { MarcaService } from 'src/app/service/marca.service';
import { ProductoService } from 'src/app/service/producto.service';
import { ProveedorService } from 'src/app/service/proveedor.service';

@Component({
  selector: 'app-productos',
  templateUrl: './app-productos.component.html',
  styleUrls: ['./app-productos.component.scss']
})
export class AppProductosComponent implements OnInit {

  edicionProductoDialog: boolean = false;
  agregarProductoDialog: boolean = false;

  @ViewChild('dt') table!: Table;

  @ViewChild('filter') filter!: ElementRef;

  loading:boolean = true;

  productoForm = this.formBuilder.group({
    nombreProducto: ['', [Validators.required]],
    codigoProducto: ['', [Validators.required]],
    descripcionProducto: [''],
    estadoProducto: [''],
    existenciaProducto: [''],
    precioUnitarioProduto: [''],
    categoriaProducto: [''],
    marcaProducto: [''],
    proveedorProducto: ['']
  });

  producto: any;
  productoRequest: Producto;
  productoMarca!: String;
  productoCategoria!: String;
  productoProveedor!: String;

  categoriaSelected: any;
  estadoSelected: any;
  marcaSelected: any;
  proveedorSelected: any;

  editarCantidad!: Producto;
  productoEditado: Producto = new Producto();
  estadoEditadoSelected: any;
  categoriaEditadoSelected: any;
  marcaEditadoSelected: any;
  proveedorEditadoSelected: any;

  estados = [
    { name: 'Activo', code: 1 },
    { name: 'Inactivo', code: 0 }
  ];

  proveedores: any;
  marcas: any;
  categorias: any;

  constructor(private productoService: ProductoService,
              private proveedorService: ProveedorService,
              private categoriaService: CategoriaService,
              private marcaService: MarcaService,
              private formBuilder: FormBuilder) {
                this.productoRequest = new Producto();
               }

  ngOnInit(): void {
    this.cargarProductos();
    this.proveedorService.getProveedores().subscribe(
      (response) => {this.proveedores = response}
    );

    this.marcaService.getMarcas().subscribe(
      (response) => {this.marcas = response}
    );

    this.categoriaService.getCategoria().subscribe(
      (response) => {this.categorias = response}
    );
  }

  cargarProductos(): void{
    this.productoService.getProductos().subscribe(
      (response) => {
        this.producto = response;


        for (let i=0; i<this.producto.length; i++){
          this.categoriaService.getCategoriaId(Number(this.producto[i].categoria)).subscribe(
            (response) => {
              this.producto[i].nombreCategoria = response.nombre;
            }
          );

          this.marcaService.getMarca(Number(this.producto[i].marca)).subscribe(
            (response) => {
              this.producto[i].nombreMarca = response.nombre;
            }
          );

          this.proveedorService.getProveedor(Number(this.producto[i].proveedor)).subscribe(
            (response) => {
              this.producto[i].nombreProveedor = response.nombre;
            }
          );
        }
        this.loading = false;
      }
    );
  }

  registrarProducto(){
    this.marcaSelected = this.productoForm.value?.marcaProducto;
    this.proveedorSelected = this.productoForm.value?.proveedorProducto;
    this.estadoSelected = this.productoForm.value?.estadoProducto;
    this.categoriaSelected = this.productoForm.value?.categoriaProducto;

    this.productoRequest.marca = this.marcaSelected.id;
    this.productoRequest.proveedor = this.proveedorSelected.id;
    this.productoRequest.nombre = String(this.productoForm.get('nombreProducto')?.value);
    this.productoRequest.codigoProducto = String(this.productoForm.get('codigoProducto')?.value);
    this.productoRequest.descripcion = String(this.productoForm.get('descripcionProducto')?.value);
    this.productoRequest.existencias = Number(this.productoForm.get('existenciaProducto')?.value);
    this.productoRequest.estado = this.estadoSelected.code;
    this.productoRequest.categoria = this.categoriaSelected.id;
    this.productoRequest.precioUnitario = Number(this.productoForm.get('precioUnitarioProduto')?.value);
    this.productoService.postProducto(this.productoRequest).subscribe(data =>{
      this.productoForm.reset(this.productoForm.value);
      this.cargarProductos();
    });
  }

  clear(table: Table) {
    table.clear();
    this.filter.nativeElement.value = '';
  }

  editarCantidadProducto(cantidadProducto: Producto){
    this.agregarProductoDialog = true;
    this.editarCantidad = {...cantidadProducto};
  }

  guardarEditarCantidad() {
    console.log(''+JSON.stringify(this.editarCantidad));
     this.productoService.putProducto(this.editarCantidad).subscribe((data)=>{
      console.log(''+JSON.stringify(data));
      this.agregarProductoDialog = false;
      this.cargarProductos();
     });

  }

  guardarEdicion(){
    console.log(''+JSON.stringify(this.editarCantidad));
    this.productoEditado = this.editarCantidad;
    this.productoEditado.estado = this.estadoEditadoSelected.code;
    this.productoEditado.categoria = this.categoriaEditadoSelected.id;
    this.productoEditado.marca = this.marcaEditadoSelected.id;
    this.productoEditado.proveedor = this.proveedorEditadoSelected.id;

    this.productoService.putProducto(this.productoEditado).subscribe((data)=>{
      console.log(''+JSON.stringify(data));
      this.edicionProductoDialog = false;
      this.cargarProductos();
     });

  }

  inactivarProducto(cantidadProducto: Producto){
    this.editarCantidad = {...cantidadProducto};
    this.editarCantidad.estado =0;
    this.productoService.putProducto(this.editarCantidad).subscribe((data)=>{
      console.log(''+JSON.stringify(data));
      this.agregarProductoDialog = false;
      this.cargarProductos();
     });
  }

  editarProducto(cantidadProducto: Producto){
    this.edicionProductoDialog = true;
    this.editarCantidad = {...cantidadProducto};
  }

}
