import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppProductosComponent } from './app-productos.component';

describe('AppProductosComponent', () => {
  let component: AppProductosComponent;
  let fixture: ComponentFixture<AppProductosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppProductosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AppProductosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
