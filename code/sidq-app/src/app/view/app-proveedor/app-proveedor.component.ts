import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Proveedor } from 'src/app/model/proveedor';
import { ProveedorService } from 'src/app/service/proveedor.service';

@Component({
  selector: 'app-app-proveedor',
  templateUrl: './app-proveedor.component.html',
  styleUrls: ['./app-proveedor.component.scss']
})
export class AppProveedorComponent implements OnInit {

  estadoSelected: any;
  proveedorRequest: Proveedor;

  proveedorForm = this.formBuilder.group({
    nombreProveedor: ['', [Validators.required]],
    direccionProveedor: ['', [Validators.required]],
    telefonoProveedor: ['', [Validators.required]],
    celularProveedor: [''],
    emailProveedor: [''],
    paginaProveedor: [''],
    codigoProveedor: [''],
    descripcionProveedor: [''],
    estadoProveedor: ['']
  });

  proveedor: any;

  estados = [
    { name: 'Activo', code: 1 },
    { name: 'Inactivo', code: 0 }
  ];

  constructor(private proveedorService: ProveedorService,
              private formBuilder: FormBuilder) {
                this.proveedorRequest = new Proveedor();
               }

  ngOnInit(): void {
    this.cargarProveedores();
  }

  cargarProveedores(){
    this.proveedorService.getProveedores().subscribe(
      (response) => {this.proveedor = response}
    );
  }

  registrarProveedor(){
    this.estadoSelected = this.proveedorForm.value?.estadoProveedor;

    this.proveedorRequest.nombre = String(this.proveedorForm.get('nombreProveedor')?.value);
    this.proveedorRequest.direccion = String(this.proveedorForm.get('direccionProveedor')?.value);
    this.proveedorRequest.telefonoContacto = String(this.proveedorForm.get('telefonoProveedor')?.value);
    this.proveedorRequest.celularContacto = String(this.proveedorForm.get('celularProveedor')?.value);
    this.proveedorRequest.emailContacto = String(this.proveedorForm.get('emailProveedor')?.value);
    this.proveedorRequest.pagina = String(this.proveedorForm.get('paginaProveedor')?.value);
    this.proveedorRequest.codigo = String(this.proveedorForm.get('codigoProveedor')?.value);
    this.proveedorRequest.descripcion = String(this.proveedorForm.get('descripcionProveedor')?.value)
    this.proveedorRequest.estado = this.estadoSelected.code;

    this.proveedorService.postProveedor(this.proveedorRequest).subscribe(data => {
      this.proveedorForm.reset(this.proveedorForm.value);
      this.proveedorForm.setValue(
        {
          nombreProveedor: '', direccionProveedor: '', telefonoProveedor: '',
          celularProveedor: '', emailProveedor: '', paginaProveedor: '',
          codigoProveedor: '', descripcionProveedor: '', estadoProveedor: ''
        }
      );
      this.cargarProveedores();
    });

  }

}
