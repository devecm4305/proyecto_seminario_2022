import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppProveedorComponent } from './app-proveedor.component';

describe('AppProveedorComponent', () => {
  let component: AppProveedorComponent;
  let fixture: ComponentFixture<AppProveedorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppProveedorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AppProveedorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
