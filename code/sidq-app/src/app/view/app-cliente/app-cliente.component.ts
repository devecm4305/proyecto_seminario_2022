import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Table } from 'primeng/table';
import { Cliente } from 'src/app/model/cliente';
import { ClienteService } from 'src/app/service/cliente.service';

@Component({
  selector: 'app-app-cliente',
  templateUrl: './app-cliente.component.html',
  styleUrls: ['./app-cliente.component.scss']
})
export class AppClienteComponent implements OnInit {

  @ViewChild('dt') table!: Table;

  @ViewChild('filter') filter!: ElementRef;

  loading:boolean = true;

  clienteDialog: boolean = false;

  cliente: any;

  clienteRequest: any;
  clienteEdit! :Cliente;

  clienteForm = this.formBuilder.group({
    primerNombre: ['', [Validators.required]],
    primerApellido: ['', [Validators.required]],
    segundoNombre: ['',],
    segundoApellido: ['',],
    direccion: ['',[Validators.required]],
    nit: ['',[Validators.required]],
    correoElectronico: ['',[Validators.required]],
    codigo: ['',[Validators.required]]
  });

  constructor(private clienteService: ClienteService,
              private formBuilder: FormBuilder) {
                this.clienteRequest = new Cliente();
               }

  ngOnInit(){
    this.cargarClientes();
  }

  cargarClientes(): void{
    this.clienteService.getClientes().subscribe(
      (response) =>{
        this.cliente = response;
        this.loading = false;
      }
    );
  }

  registrarCliente(){
    this.clienteRequest.codigo = String(this.clienteForm.get('codigo')?.value);
    this.clienteRequest.primerNombre = String(this.clienteForm.get('primerNombre')?.value);
    this.clienteRequest.segundoNombre = String(this.clienteForm.get('segundoNombre')?.value);
    this.clienteRequest.primerApellido = String(this.clienteForm.get('primerApellido')?.value);
    this.clienteRequest.segundoApellido = String(this.clienteForm.get('segundoApellido')?.value);
    this.clienteRequest.nit = String(this.clienteForm.get('nit')?.value);
    this.clienteRequest.direccion = String(this.clienteForm.get('direccion')?.value);
    this.clienteRequest.correoElectronico = String(this.clienteForm.get('correoElectronico')?.value);
    console.log('  --  '+JSON.stringify(this.clienteRequest));

    this.clienteService.postCliente(this.clienteRequest).subscribe(data =>{
      this.clienteForm.setValue({codigo: '', primerNombre: '', segundoNombre: '', primerApellido:'', segundoApellido:'', nit:'', correoElectronico: '', direccion: ''});
      this.cargarClientes();
    });
  }

  editCliente(clienteE: Cliente) {
    this.clienteEdit = {...clienteE};
    this.clienteDialog = true;
  }

  hideDialog(){
    this.clienteDialog = false;
  }

  update(){
    this.clienteService.putCliente(this.clienteEdit).subscribe(data => {
      this.cargarClientes();
      this.clienteDialog = false;
    });
  }

  clear(table: Table) {
    table.clear();
    this.filter.nativeElement.value = '';
  }

}
