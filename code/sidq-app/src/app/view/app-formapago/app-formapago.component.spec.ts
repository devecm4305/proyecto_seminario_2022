import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppFormapagoComponent } from './app-formapago.component';

describe('AppFormapagoComponent', () => {
  let component: AppFormapagoComponent;
  let fixture: ComponentFixture<AppFormapagoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppFormapagoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AppFormapagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
