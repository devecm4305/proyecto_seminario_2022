import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { FormaPago } from 'src/app/model/formapago';
import { FormapagoService } from 'src/app/service/formapago.service';

@Component({
  selector: 'app-app-formapago',
  templateUrl: './app-formapago.component.html',
  styleUrls: ['./app-formapago.component.scss']
})
export class AppFormapagoComponent implements OnInit {

  formaPagoDialog: boolean = false;

  formaPagoForm = this.formBuilder.group({
    nombre: ['',[Validators.required]],
    estado: ['',[Validators.required]],
    descripcion: ['']
  });

  formaPago: any;
  formaPagoRequest: FormaPago;
  formaPagoEdit!: FormaPago;

  estadoSelected: any;

  estados = [
    { name: 'Activo', code: 1 },
    { name: 'Inactivo', code: 0 }
  ];

  constructor(private formaPagoService: FormapagoService,
              private formBuilder: FormBuilder) {
                this.formaPagoRequest = new FormaPago();
              }

  ngOnInit(): void {
    this.cargarFormasdePago();
  }

  cargarFormasdePago() {
    this.formaPagoService.getFormasPago().subscribe(
      (response) => {
        this.formaPago = response;
      }
    );
  }

  registraFormadePago() {
    this.estadoSelected = this.formaPagoForm.value?.estado;
    this.formaPagoRequest.nombre = String(this.formaPagoForm.get('nombre')?.value);
    this.formaPagoRequest.estado = this.estadoSelected.code;
    this.formaPagoRequest.descripcion = String(this.formaPagoForm.get('descripcion')?.value);

    this.formaPagoService.postFormaPago(this.formaPagoRequest).subscribe(data => {
      this.formaPagoForm.setValue({nombre : '', estado: '', descripcion: ''})
      this.cargarFormasdePago();
    });
  }

  update(){
    this.formaPagoEdit.estado = this.estadoSelected.code;
    this.formaPagoService.putFormaPago(this.formaPagoEdit).subscribe(data => {
      this.cargarFormasdePago();
      this.formaPagoDialog = false;
    });
  }

  editFormaPago(formaPagoE: FormaPago){
    this.formaPagoEdit = {...formaPagoE};
    this.formaPagoDialog = true;
    this.estadoSelected = formaPagoE.estado;
  }

  hideDialog(){
    this.formaPagoDialog = false;
  }

}
