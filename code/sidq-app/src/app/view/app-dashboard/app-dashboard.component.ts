import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataSetReport } from 'src/app/model/datareporte';
import { FacturaService } from 'src/app/service/factura.service';
import { ProductoService } from 'src/app/service/producto.service';

@Component({
  selector: 'app-app-dashboard',
  templateUrl: './app-dashboard.component.html',
  styleUrls: ['./app-dashboard.component.scss']
})
export class AppDashboardComponent implements OnInit {

  lineData: any;

    barData: any;

    barDataCliente: any;

    pieData: any;

    polarData: any;

    radarData: any;

    lineOptions: any;

    barOptions: any;
    barClienteOptions: any;

    pieOptions: any;

    polarOptions: any;

    radarOptions: any;

  constructor(private router: Router, private productoService: ProductoService,
              private facturaService: FacturaService) { }

  ngOnInit(): void {

    this.lineData = {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [
          {
              label: 'First Dataset',
              data: [65, 59, 80, 81, 56, 55, 40],
              fill: false,
              backgroundColor: '#2f4860',
              borderColor: '#2f4860',
              tension: .4
          },
          {
              label: 'Second Dataset',
              data: [28, 48, 40, 19, 86, 27, 90],
              fill: false,
              backgroundColor: '#00bb7e',
              borderColor: '#00bb7e',
              tension: .4
          }
      ]
  };

  this.facturaService.getProductosMes().subscribe(data =>{
    let results: any = data;
    let days: any[]=[];
    let cant: any[]=[];
    let names: any[]=[];
    let dataS: DataSetReport[][]=[];
    var listado: any[]  | undefined;

    for (let i=0; i< results.length; i++){
      days.push(results[i].fechaCreacion);
      names.push(results[i].nombre);
      cant.push(results[i].cantidad);
    }

    this.barData = {
      labels: names,
      datasets: [
        {
          label: 'Ventas del Mes Actual',
          data: cant,
          backgroundColor: '#2f4860',
        }
      ]
    }


  });


  /*this.barData = {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [
          {
              label: 'Lapiz',
              backgroundColor: '#2f4860',
              data: [65, 59, 80, 81, 56, 55, 40]
          },
          {
              label: 'Hojas',
              backgroundColor: '#00bb7e',
              data: [28, 48, 40, 19, 86, 27, 90]
          },
          {
            label: 'Bolsón',
            backgroundColor: '#00457a',
            data: [40, 31, 10, 29, 36, 67, 10]
          }
      ]
  };*/

  this.barDataCliente = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
        {
            label: 'Cliente 1',
            backgroundColor: '#2f4860',
            data: [65, 59, 80, 81, 56, 55, 40]
        },
        {
            label: 'Cliente 2',
            backgroundColor: '#00bb7e',
            data: [28, 48, 40, 19, 86, 27, 90]
        },
        {
          label: 'Cliente 3',
          backgroundColor: '#00457a',
          data: [40, 31, 10, 29, 36, 67, 10]
        }
    ]
 };

 this.productoService.getProductosExistencias().subscribe(data =>{
  let results: any = data;
  let prodNames:  any[]=[];
  let prodExist:  any[]=[];
  for (let i=0; i< results.length; i++){
    prodNames.push(results[i].nombre);
    prodExist.push(results[i].existencias);
  }
  this.pieData = {
    labels: prodNames,
    datasets: [
        {
            data: prodExist,
            backgroundColor: [
                "#FF6384",
                "#36A2EB",
                "#FFCE56",
                "#3383FF",
                "#33FF39",
                "#C825CD"
            ],
            hoverBackgroundColor: [
                "#FF6384",
                "#36A2EB",
                "#FFCE56",
                "#3383FF",
                "#33FF39",
                "#C825CD"
            ]
        }
    ]
};

 });


  this.polarData = {
      datasets: [{
          data: [
              11,
              16,
              7,
              3,
              14
          ],
          backgroundColor: [
              "#FF6384",
              "#4BC0C0",
              "#FFCE56",
              "#E7E9ED",
              "#36A2EB"
          ],
          label: 'My dataset'
      }],
      labels: [
          "Red",
          "Green",
          "Yellow",
          "Grey",
          "Blue"
      ]
  };

  this.radarData = {
      labels: ['Eating', 'Drinking', 'Sleeping', 'Designing', 'Coding', 'Cycling', 'Running'],
      datasets: [
          {
              label: 'My First dataset',
              backgroundColor: 'rgba(179,181,198,0.2)',
              borderColor: 'rgba(179,181,198,1)',
              pointBackgroundColor: 'rgba(179,181,198,1)',
              pointBorderColor: '#fff',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: 'rgba(179,181,198,1)',
              data: [65, 59, 90, 81, 56, 55, 40]
          },
          {
              label: 'My Second dataset',
              backgroundColor: 'rgba(255,99,132,0.2)',
              borderColor: 'rgba(255,99,132,1)',
              pointBackgroundColor: 'rgba(255,99,132,1)',
              pointBorderColor: '#fff',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: 'rgba(255,99,132,1)',
              data: [28, 48, 40, 19, 96, 27, 100]
          }
      ]
  };
  }

  navigateToFacturas($event: any){
    this.router.navigateByUrl('/sidq/facturas');
  }

  navigateToClientes($event: any){
    this.router.navigateByUrl('/sidq/clientes');
  }

  navigateToProducto($event: any){
    this.router.navigateByUrl('/sidq/productos');
  }

  navigateToCompras($event: any){
    this.router.navigateByUrl('/sidq/compras');
  }

}
