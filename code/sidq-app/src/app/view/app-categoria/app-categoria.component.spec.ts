import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppCategoriaComponent } from './app-categoria.component';

describe('AppCategoriaComponent', () => {
  let component: AppCategoriaComponent;
  let fixture: ComponentFixture<AppCategoriaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppCategoriaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AppCategoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
