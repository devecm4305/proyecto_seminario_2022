import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Categoria } from 'src/app/model/categoria';
import { CategoriaService } from 'src/app/service/categoria.service';

@Component({
  selector: 'app-app-categoria',
  templateUrl: './app-categoria.component.html',
  styleUrls: ['./app-categoria.component.scss']
})
export class AppCategoriaComponent implements OnInit {

  categoriaDialog: boolean = false;

  categoriaForm = this.formBuilder.group({
    nombreCategoria: ['', [Validators.required]],
    descripcionCategoria: ['']
  });

  categoria: any;
  categoriaRequest: Categoria;
  categoriaEdit!: Categoria;

  constructor(private categoriaService: CategoriaService,
              private formBuilder: FormBuilder) {
                this.categoriaRequest = new Categoria();
              }

  ngOnInit(): void {
    this.cargarCategorias();
  }

  cargarCategorias(){
    this.categoriaService.getCategoria().subscribe(
      (response) => { this.categoria = response; }
    );
  }

  registrarCategoria(){
    this.categoriaRequest.nombre = String(this.categoriaForm.get('nombreCategoria')?.value);
    this.categoriaRequest.descripcion = String(this.categoriaForm.get('descripcionCategoria')?.value);
    this.categoriaService.postCategoria(this.categoriaRequest).subscribe(data => {
      this.categoriaForm.reset(this.categoriaForm.value);
      this.categoriaForm.setValue({nombreCategoria: '', descripcionCategoria: ''});
      this.cargarCategorias();
    });
  }

  update(){
    this.categoriaService.putCategoria(this.categoriaEdit).subscribe(data => {
      this.cargarCategorias();
      this.categoriaDialog = false;
    });
  }

  editCategoria(categoriaE: Categoria){
    this.categoriaEdit = {...categoriaE};
    this.categoriaDialog = true;
  }

  hideDialog() {
    this.categoriaDialog = false;
  }

}
