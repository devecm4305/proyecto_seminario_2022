import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppCompraComponent } from './app-compra.component';

describe('AppCompraComponent', () => {
  let component: AppCompraComponent;
  let fixture: ComponentFixture<AppCompraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppCompraComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AppCompraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
