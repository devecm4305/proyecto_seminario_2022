import { Component, OnInit } from '@angular/core';
import { Compra } from 'src/app/model/compra';
import { DetalleCompra } from 'src/app/model/detallecompra';
import { CompraService } from 'src/app/service/compra.service';
import { FormapagoService } from 'src/app/service/formapago.service';
import { MarcaService } from 'src/app/service/marca.service';
import { ProductoService } from 'src/app/service/producto.service';
import { ProveedorService } from 'src/app/service/proveedor.service';

@Component({
  selector: 'app-app-compra',
  templateUrl: './app-compra.component.html',
  styleUrls: ['./app-compra.component.scss']
})
export class AppCompraComponent implements OnInit {

  fechaFactura = new Date().getTime().toString();

  compra: any;
  compraDetalle: any;
  productoDetalle: any;
  compraReimp: Compra= new Compra();
  nombreProveedor!: string;

  crearCompraDetalleDialog: boolean = false;
  listDetalleDialog: boolean = false;

  producto!: string;
  results: any = [];
  filteredProducts!: any[];
  filteredNameProducts!: any[];

  selectedProduct!: any;
  selectedNameProduct!: any;

  products!: any[];

  proveedores!: any;
  formasPago!: any;
  proveedor!: any;
  formaPago!: any;

  marcas!: any;
  marca!: any;

  total: number = 0;
  subTotal: number = 0;
  precioUnitario: number =0;
  cantidad: number = 0;

  detalle: any[]=[];
  detalleItems: DetalleCompra[]=[];

  nombreProducto: any;

  listDetalleCompra: any;

  constructor(private productoService: ProductoService,
             private proveedorService: ProveedorService,
             private formaPagoService: FormapagoService,
             private marcaService: MarcaService,
             private compraService: CompraService) {
              this.total = 0;
              }

  ngOnInit(): void {
    this.selectedProduct= 'PAPE-BOND-001';
    this.total = 0;
    this.proveedorService.getProveedores().subscribe(
      (response) => {this.proveedores = response}
    );
    this.formaPagoService.getFormasPago().subscribe(
      (response) =>{
        this.formasPago = response;
      }
    );
    this.marcaService.getMarcas().subscribe(
      (response)=> {
        this.marcas = response;
      }
    );

    this.cargarCompras();

    this.productoService.getProductos().subscribe(
      (response) => {
        this.results = response;
      });

  }

  agregarDetalle(){

    //this.detalle.push(this.marca,this.precioUnitario,this.subTotal);

    this.subTotal = Number(this.precioUnitario) * Number(this.cantidad);
    this.total = Number(this.subTotal) + Number(this.total);

    let item = new DetalleCompra();
    item.marca = this.marca.nombre;
    item.precioUnitario = this.precioUnitario;
    item.subTotal = this.subTotal;
    item.cantidad = this.cantidad;
    item.codigoProducto = this.selectedProduct.codigoProducto;
    item.productoId = this.selectedProduct.id?this.selectedProduct.id:this.selectedNameProduct.id;
    item.producto = this.selectedProduct.nombre?this.selectedProduct.nombre:this.selectedNameProduct.nombre;
    this.detalleItems.push(item);

    this.precioUnitario=0;
    this.marca="";
    this.selectedProduct="";
    this.cantidad=0;
  }

  createCompraDetalle(){
    this.crearCompraDetalleDialog = true;
  }

  getProveedores(){
    this.proveedorService.getProveedores().subscribe();
  }

  search(event: { query: any; }){

    this.productoService.getProductos().subscribe(
      (response) => {
        this.results = response;
      });

    let filtered: any[] = [];
    let query = event.query;
    let nombre = '';
    for (let i = 0; i < this.results.length; i++) {
      let prod = this.results[i];
      if (prod.codigoProducto.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(prod);
      }
    }
    this.filteredProducts = filtered;

  }

  searchByName(event: { query: any; }){
    this.selectedProduct= 'PAPE-BOND-001';

    let filtered: any[] = [];
    let query = event.query;
    let nombre = '';
    for (let i = 0; i < this.results.length; i++) {
      let prod = this.results[i];
      if (prod.nombre.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(prod);

      }
    }
    this.filteredNameProducts = filtered;

    for (let i = 0; i < this.results.length; i++) {
      let prod = this.results[i];
      if (prod.nombre.toLowerCase().indexOf(this.selectedNameProduct.toLowerCase())==0) {
        console.log('---  codigo :  '+this.selectedNameProduct);
        console.log('--- codproducto:  '+prod.codigoProducto);
        this.selectedProduct = prod.codigoProducto;

      }
    }
  }

  cambioProducto(){
    for (let i = 0; i < this.results.length; i++) {
      let prod = this.results[i];
      if (prod.nombre == this.selectedNameProduct) {
        this.selectedProduct = prod.codigoProducto;
      }
    }
  }

  removerProductoDetalle(element: string) {
    this.detalleItems.forEach((value, index)=> {
      if (value.codigoProducto==element) this.detalleItems.splice(index,1);
    });
    this.total = 0;
    this.detalleItems.forEach((value, index)=> {
      this.total += value.subTotal;
    });
  }


  obtenerProductos(){
    this.productoService.getProductos().subscribe(
      (response) => {
        console.log('-- -- -- '+JSON.stringify(response));
      }
    );
  }

  cargarCompras(): void {
    this.compraService.getCompras().subscribe(
      (response) => {
        this.compra = response;
        for(let i=0; i<this.compra.length;i++){
          this.formaPagoService.getFormaPagoId(Number(this.compra[i].formaPago)).subscribe(
            (response) => {
              this.compra[i].formaPago = response.nombre;
            }
          );
          this.compraService.getDetalleByIdCompra(Number(this.compra[i].id)).subscribe(
            (response) => {
              this.compraDetalle = response;
              if(undefined != this.compraDetalle[0]){
                this.productoService.getProducto(this.compraDetalle[0].producto).subscribe((data)=>{
                  this.proveedorService.getProveedor(data.proveedor).subscribe((dataProveedor)=>{
                    console.log('->',JSON.stringify(dataProveedor));
                    this.compra[i].proveedor=dataProveedor.nombre;
                  });
                });
                /*this.proveedorService.getProveedor(this.compraDetalle[0].producto).subscribe((data)=>{
                  console.log('****',data);
                  this.compra[i].proveedor=data.nombre;
                });*/
              }
            }
          );
        }
      }
    );
  }

  registraCompra(){
    let compra = new Compra();
    compra.usuario=1;
    compra.total = this.total;
    compra.codigo = '0000';
    compra.formaPago = this.formaPago.id;
    this.compraService.postCompra(compra).subscribe(data => {
      for(let i=0; i<this.detalleItems.length; i++){
        let detCompra = new DetalleCompra();
        detCompra.cantidad = this.detalleItems[i].cantidad;
        detCompra.subTotal = this.detalleItems[i].subTotal;
        detCompra.compra = data.id;
        detCompra.precioUnitario = this.detalleItems[i].precioUnitario;
        detCompra.producto = this.detalleItems[i].productoId;
        console.log('----- -----  --',JSON.stringify(this.detalleItems[i]));
        this.compraService.postDetalleCompra(detCompra).subscribe(data => {

        });
      }
      this.cargarCompras();
      this.crearCompraDetalleDialog = false;
      this.detalleItems = [];
      this.total = 0;
      this.formaPago = '';
      this.proveedor = '';
      this.cantidad = 0;
      this.selectedProduct = '';
    });
  }

  cargarListadoCompra(compraR: Compra, nombreP: string){
    this.nombreProveedor = nombreP;
    this.compraReimp = compraR;
    this.listDetalleDialog = true;
    this.compraService.getDetalleByIdCompra(this.compraReimp.id).subscribe(
      (response) => {
        this.listDetalleCompra = response;
        for (let i=0; i < this.listDetalleCompra.length; i++){
          this.productoService.getProducto(Number(this.listDetalleCompra[i].producto)).subscribe(
            (response) => {
              this.listDetalleCompra[i].codigoProducto = response.codigoProducto;
              this.listDetalleCompra[i].producto = response.nombre;
              this.marcaService.getMarca(Number(response.marca)).subscribe(
                (response) => {
                  this.listDetalleCompra[i].marca = response.nombre;
                }
              );
            }
          );
        }
      }
    );
  }

  obtenerPrecioNombre(){
    if(this.selectedNameProduct.precioUnitario !== undefined){

      this.selectedProduct = "";
    }
  }

  print() {
    window.print();
  }

}
