import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Caja } from 'src/app/model/caja';
import { Marca } from 'src/app/model/marca';
import { MarcaService } from 'src/app/service/marca.service';

@Component({
  selector: 'app-app-marca',
  templateUrl: './app-marca.component.html',
  styleUrls: ['./app-marca.component.scss']
})
export class AppMarcaComponent implements OnInit {

  productDialog: boolean = false;

  marcaDialog: boolean = false;

  estadoSelected: any;

  marcaForm = this.formBuilder.group({
    nombreMarca:  ['', [Validators.required]],
    estadoMarca:  ['', [Validators.required]],
    descripcionMarca: [''],
    calificacionMarca: ['']
  });

  marca: any;
  marcaRequest :Marca;
  marcaEdit! :Marca;

  estados = [
    { name: 'Activo', code: 1 },
    { name: 'Inactivo', code: 0 }
  ];

  constructor(private marcaService: MarcaService,
              private formBuilder: FormBuilder) {
                this.marcaRequest = new Marca();
              }

  ngOnInit(): void {
    this.cargarMarcas();
  }

  cargarMarcas(): void{
    this.marcaService.getMarcas().subscribe(
      (response) => {this.marca = response;}
    );
  }

  registrarMarca(){
    this.estadoSelected = this.marcaForm.value?.estadoMarca;
    this.marcaRequest.nombre = String(this.marcaForm.get('nombreMarca')?.value);
    this.marcaRequest.estado = this.estadoSelected.code;
    this.marcaRequest.descripcion = String(this.marcaForm.get('descripcionMarca')?.value);
    this.marcaRequest.calificacion = Number(this.marcaForm.get('calificacionMarca')?.value);

    this.marcaService.postMarcas(this.marcaRequest).subscribe(data => {
      this.marcaForm.reset(this.marcaForm.value);
      this.marcaForm.setValue({nombreMarca: '', estadoMarca: '',descripcionMarca: '', calificacionMarca:''});
      this.cargarMarcas();
    });
  }

  editMarca(marcaE: Marca) {
    this.marcaEdit = {...marcaE};
    this.productDialog = true;
  }

  hideDialog() {
    this.productDialog = false;
  }

}
