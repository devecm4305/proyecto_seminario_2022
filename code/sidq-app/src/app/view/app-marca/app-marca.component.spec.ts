import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppMarcaComponent } from './app-marca.component';

describe('AppMarcaComponent', () => {
  let component: AppMarcaComponent;
  let fixture: ComponentFixture<AppMarcaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppMarcaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AppMarcaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
