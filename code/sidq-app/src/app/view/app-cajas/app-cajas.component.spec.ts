import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppCajasComponent } from './app-cajas.component';

describe('AppCajasComponent', () => {
  let component: AppCajasComponent;
  let fixture: ComponentFixture<AppCajasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppCajasComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AppCajasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
