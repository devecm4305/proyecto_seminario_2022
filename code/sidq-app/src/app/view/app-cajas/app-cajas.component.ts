import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Caja } from 'src/app/model/caja';
import { CajaServiceService } from 'src/app/service/caja-service.service';

@Component({
  selector: 'app-app-cajas',
  templateUrl: './app-cajas.component.html',
  styleUrls: ['./app-cajas.component.scss']
})
export class AppCajasComponent implements OnInit {

  productDialog: boolean = false;

  estadoSelected: any;

  estadoCode!: number;

  cajaForm = this.formBuilder.group({
    codigoCaja: ['', [Validators.pattern('^[0-9]*$'), Validators.required]],
    estadoCaja: ['', [Validators.required]]
  });

  caja: any;
  cajaRequest :Caja;
  cajaEdit! :Caja;

  estados = [
    { name: 'Activo', code: 1 },
    { name: 'Inactivo', code: 0 }
  ];

  constructor(private cajaService: CajaServiceService,
              private formBuilder: FormBuilder) {
                this.cajaRequest = new Caja();
              }

  ngOnInit(){
    this.cargarCajas();
  }

  cargarCajas(){
    this.cajaService.getCajas().subscribe(
      (response) => { this.caja = response;}
    );
  }

  registrarCaja(){
    this.estadoSelected = this.cajaForm.value?.estadoCaja;

    this.cajaRequest.codigo = Number(this.cajaForm.get('codigoCaja')?.value);
    this.cajaRequest.estado = this.estadoSelected.code;

    this.cajaService.postCajas(this.cajaRequest).subscribe(data => {
      this.cajaForm.reset(this.cajaForm.value);
      this.cajaForm.setValue({codigoCaja: '', estadoCaja: ''});
      this.cargarCajas();
    });

  }

  hideDialog() {
    this.productDialog = false;
  }

  update(){
    this.cajaEdit.estado = this.estadoSelected.code;
    this.cajaService.putCajas(this.cajaEdit).subscribe(data => {
      console.log(data);
      this.cargarCajas();
      this.productDialog = false;
    });
  }

  editProduct(cajaE: Caja) {
    this.cajaEdit = {...cajaE};
    this.estadoSelected = cajaE.estado;
    this.productDialog = true;
  }

}
