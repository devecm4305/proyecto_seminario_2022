import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Message, MessageService, SelectItem } from "primeng/api";
import { Cliente } from "src/app/model/cliente";
import { Factura } from "src/app/model/factura";
import { DetalleFactura } from "src/app/model/facturadetalle";
import { Producto } from "src/app/model/producto";
import { CajaServiceService } from "src/app/service/caja-service.service";
import { ClienteService } from "src/app/service/cliente.service";
import { PrintFacturaService } from "src/app/service/data/print-factura.service";
import { FacturaService } from "src/app/service/factura.service";
import { FormapagoService } from "src/app/service/formapago.service";
import { ProductoService } from "src/app/service/producto.service";

@Component({
  selector: 'app-factura-stepone',
  templateUrl: './app-factura-stepone.component.html',
  styles: [`
  :host ::ng-deep .p-message {
    margin-left: .25em;
  }

      :host ::ng-deep .p-toast{
          z-index:99999;
      }
  `],
  providers: [MessageService]
})
export class AppFacturaSetpOneComponent implements OnInit {

  clienteInfo: Cliente = new Cliente();
  nitBusqueda: string = '';
  codigoBusqueda: string = '';
  nombreCompletoCliente: string = '';

  cajas: SelectItem[] = [];
  selectedDrop!: SelectItem;

  selectedArticulo!: SelectItem;
  selectedArticulo2!: SelectItem;
  selectedArticulo3!: SelectItem;

  precioUnitario1: number = 0;
  precioUnitario2: number = 0;
  precioUnitario3: number = 0;

  cantidad1: number=0;
  cantidad2: number=0;
  cantidad3: number=0;

  detalle1!: number;
  detalle2!: number;
  detalle3!: number;

  cajasResponse:  any;
  totalMuestra: number = 0.00;

  articulosResponse: any;
  articulos: SelectItem[] = [];

  crearFacturaDetalleDialog: boolean = false;

  primerNombre: string ='';
  primerApellido: string ='';

  detalleItems: DetalleFactura[]=[];

  total: number = 0;
  subTotal: number = 0;
  precioUnitario: number =0;
  cantidad: number = 0;

  crearVentaDetalleDialog: boolean = false;
  visualizarFactura: boolean = false;
  results: any = [];
  resultsName: any = [];

  producto!: string;
  filteredProducts!: any[];
  filteredNameProducts!: any[];

  selectedProduct!: any;
  selectedNameProduct!: any;

  marcas!: any;
  marca!: any;

  formasPago!: any;
  formaPago!: any;
  caja !: any;

  msgs: Message[] = [];

  constructor(private router: Router,
              private facturaService: FacturaService,
              private clienteService: ClienteService,
              private cajaService: CajaServiceService,
              private productoService: ProductoService,
              private formaPagoService: FormapagoService,
              private _dataFactura: PrintFacturaService){}

  ngOnInit(): void {
    this.obtenerCajas();
    this.obtenerProductos();
    this.formaPagoService.getFormasPago().subscribe(
      (response) =>{
        this.formasPago = response;
      }
    );
  }

  recargar(){
    window.location.reload();
  }

  obtenerCajas(){
    this.cajaService.getCajas().subscribe(
      (response) =>{
        this.cajasResponse = response;
        for(let i=0; i<this.cajasResponse.length;i++){
          this.cajas.push({label:this.cajasResponse[i].codigo,value: this.cajasResponse[i].id});
        }
      }
    );
  }

  obtenerProductos(){
    this.productoService.getProductos().subscribe(
      (response) => {
        this.articulosResponse = response;
        for(let i=0; i<this.articulosResponse.length; i++){
          this.articulos.push({label: this.articulosResponse[i].nombre, value: this.articulosResponse[i].id});
        }
      }
    );
  }

  subTotal1(){
    this.detalle1 = Number(this.precioUnitario1) * Number(this.cantidad1);
  }

  subTotal2(){
    this.detalle2 = Number(this.precioUnitario2) * Number(this.cantidad2);
  }

  subTotal3(){
    this.detalle3 = Number(this.precioUnitario3) * Number(this.cantidad3);
  }

  obtieneInfoProducto1(){
    let infoArt1: any;
    this.productoService.getProducto(Number(this.selectedArticulo)).subscribe(
      (response)=>{
        infoArt1 = response;
        this.precioUnitario1 = infoArt1.precioUnitario;
      }
    );
  }

  obtieneInfoProducto2(){
    let infoArt1: any;
    this.productoService.getProducto(Number(this.selectedArticulo2)).subscribe(
      (response)=>{
        infoArt1 = response;
        this.precioUnitario2 = infoArt1.precioUnitario;
      }
    );
  }

  obtieneInfoProducto3(){
    let infoArt1: any;
    this.productoService.getProducto(Number(this.selectedArticulo3)).subscribe(
      (response)=>{
        infoArt1 = response;
        this.precioUnitario3 = infoArt1.precioUnitario;
      }
    );
  }

  obtenerClienteByNit(){
    this.clienteService.getClienteByNIT(this.nitBusqueda).subscribe(
      (response)=>{
        this.clienteInfo = response;
        this.nombreCompletoCliente = this.clienteInfo.primerNombre.toUpperCase()+' '
                                    +this.clienteInfo.segundoNombre.toUpperCase()+' '
                                    +this.clienteInfo.primerApellido.toUpperCase()+' '
                                    +this.clienteInfo.segundoApellido.toUpperCase();
      }
    );
  }

  obtenerClienteByCodigo(){
    this.clienteService.getClienteByCodigo(this.codigoBusqueda).subscribe(
      (response)=>{
        this.clienteInfo = response;
        this.nombreCompletoCliente = this.clienteInfo.primerNombre.toUpperCase()+' '
                                    +this.clienteInfo.segundoNombre.toUpperCase()+' '
                                    +this.clienteInfo.primerApellido.toUpperCase()+' '
                                    +this.clienteInfo.segundoApellido.toUpperCase();
      }
    );
  }

  obtenerClienteByNombreApellido(){
    this.clienteService.getClienteByNombreApellido(this.primerNombre, this.primerApellido).subscribe(
      (response)=>{
        //console.log('*-*-*-*-*-*- '+JSON.stringify(response[0]));
        this.clienteInfo = response[0];
        this.nombreCompletoCliente = this.clienteInfo.primerNombre.toUpperCase()+' '
        +this.clienteInfo.segundoNombre.toUpperCase()+' '
        +this.clienteInfo.primerApellido.toUpperCase()+' '
        +this.clienteInfo.segundoApellido.toUpperCase();
      }
    );
  }

  createFacturaDetalle(){
    //this.crearFacturaDetalleDialog = true;
    this.crearVentaDetalleDialog = true;
    this.obtenerProductos();
  }

  cierraFacturaDetalle(){
    this.crearFacturaDetalleDialog = false;
    this.totalMuestra = this.detalle1 + this.detalle2 +this.detalle3;
  }

  navigateToNext(){
    this.router.navigate(['sidq/facturas/pasoDos'])
  }


  removerProductoDetalle(element: string) {
    this.detalleItems.forEach((value, index)=> {
      if (value.codigoProducto==element) this.detalleItems.splice(index,1);
    });
    this.total = 0;
    this.detalleItems.forEach((value, index)=> {
      this.total += value.subTotal;
    });
  }

  registraVenta(){
    //this.router.navigateByUrl('/sidq/factura-print');
    let venta = new Factura();
    venta.usuario=1;
    venta.monto = this.total;
    //venta.codigo = '0000';
    venta.formaPago = this.formaPago.id;
    venta.autorizacion = 'AAW4EE-8498EE-646';
    venta.serie = '0001231-0001';
    venta.caja = this.formaPago.id;
    venta.cliente = this.clienteInfo.id;
    venta.estado =1;
    this.facturaService.postFactura(venta).subscribe(data => {
      for(let i=0; i<this.detalleItems.length; i++){
        let detFactura = new DetalleFactura();
        detFactura.cantidad = this.detalleItems[i].cantidad;
        detFactura.subTotal = this.detalleItems[i].subTotal;
        detFactura.factura = data.id;
        detFactura.descuento = 1;
        detFactura.precioUnitario = this.detalleItems[i].precioUnitario;
        detFactura.producto = this.detalleItems[i].productoId;
        this.facturaService.postDetalleFactura(detFactura).subscribe(data => {
          this.productoService.getProducto(detFactura.producto).subscribe(data => {
            let productoTmp = new Producto();
            productoTmp.id = data.id;
            productoTmp.nombre = data.nombre;
            productoTmp.descripcion = data.descripcion;
            productoTmp.estado = data.estado;
            productoTmp.precioUnitario = data.precioUnitario;
            productoTmp.categoria = data.categoria;
            productoTmp.marca = data.marca;
            productoTmp.proveedor = data.proveedor;
            productoTmp.codigoProducto = data.codigoProducto;
            productoTmp.existencias = Number(data.existencias) - Number(detFactura.cantidad);
            this.productoService.putProducto(productoTmp).subscribe(data => {

            });
          });

        });
      }
      venta.listDetalle = this.detalleItems;
      this._dataFactura.setDataStream(venta);
      //this.cargarCompras();
      this.crearVentaDetalleDialog = false;
      this.detalleItems = [];
      this.total = 0;
      this.formaPago = '';
      //this.proveedor = '';
      this.cantidad = 0;
      this.selectedProduct = '';
    });

    this.visualizarFactura = true;
  }

  construirDetalle(){
    this.subTotal = Number(this.precioUnitario) * Number(this.cantidad);
    this.total = Number(this.subTotal) + Number(this.total);
    let item = new DetalleFactura();
    //item.marca = this.marca.nombre;
    item.precioUnitario = this.precioUnitario;
    item.subTotal = this.subTotal;
    item.cantidad = this.cantidad;
    item.codigoProducto = this.selectedProduct.codigoProducto?this.selectedProduct.codigoProducto:this.selectedNameProduct.codigoProducto;
    item.productoId = this.selectedProduct.id?this.selectedProduct.id:this.selectedNameProduct.id;
    item.producto = this.selectedProduct.nombre?this.selectedProduct.nombre:this.selectedNameProduct.nombre;
    this.detalleItems.push(item);

    this.precioUnitario=0;
    this.marca="";
    this.selectedProduct="";
    this.cantidad=0;
    this.selectedNameProduct ="";
  }

  agregarDetalle(){

    if(this.cantidad==0){
      this.showErrorViaMessages('error','Error en Detalle de Compra', 'No se ha Establecido una Cantidad.', false);
    }else{
      if(undefined!=this.selectedProduct){
        let exist = this.selectedProduct?this.selectedProduct.existencias:this.selectedNameProduct.existencias;
        if(exist<this.cantidad){
          this.showErrorViaMessages('error','Error en Detalle de Compra', 'La Cantidad Requerida Excede el Disponible del Producto.', false);
        }else{
          this.construirDetalle();
          this.msgs = [];
        }
      }
    }

    /*let item = new DetalleFactura();
    //item.marca = this.marca.nombre;
    item.precioUnitario = this.precioUnitario;
    item.subTotal = this.subTotal;
    item.cantidad = this.cantidad;
    item.codigoProducto = this.selectedProduct.codigoProducto?this.selectedProduct.codigoProducto:this.selectedNameProduct.codigoProducto;
    item.productoId = this.selectedProduct.id;
    item.producto = this.selectedProduct.nombre?this.selectedProduct.nombre:this.selectedNameProduct.nombre;
    this.detalleItems.push(item);

    this.precioUnitario=0;
    this.marca="";
    this.selectedProduct="";
    this.cantidad=0;
    this.selectedNameProduct ="";*/
  }

  search(event: { query: any; }){

    this.productoService.getProductos().subscribe(
      (response) => {
        this.results = response;
      });

    let filtered: any[] = [];
    let query = event.query;
    let nombre = '';
    for (let i = 0; i < this.results.length; i++) {
      let prod = this.results[i];
      if (prod.codigoProducto.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(prod);
      }
    }
    this.filteredProducts = filtered;

  }

  searchByName(event: { query: any; }){

    this.productoService.getProductos().subscribe(
      (response) => {
        this.resultsName = response;
      });

    let filtered: any[] = [];
    let query = event.query;
    let nombre = '';
    for (let i = 0; i < this.resultsName.length; i++) {
      let prod = this.resultsName[i];
      if (prod.nombre.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(prod);
      }
    }
    this.filteredNameProducts = filtered;

  }

  obtenerPrecio(){
    if(this.selectedProduct.precioUnitario !== undefined){
      this.precioUnitario = this.selectedProduct.precioUnitario;
    }
  }

  obtenerPrecioNombre(){
    if(this.selectedNameProduct.precioUnitario !== undefined){
      this.precioUnitario = this.selectedNameProduct.precioUnitario;
      //this.selectedProduct = this.selectedNameProduct.codigoProducto;
      this.selectedProduct = "";
    }
  }

  cerrarDetalle(){
    this.crearVentaDetalleDialog = false;
  }

  showErrorViaMessages(severityTxt: string, summaryTxt: string, detailTxt: string, closeMessage: boolean) {
    this.msgs = [];
    this.msgs.push({ sticky: true,severity: severityTxt, summary: summaryTxt, detail: detailTxt, closable: closeMessage});
  }

}
