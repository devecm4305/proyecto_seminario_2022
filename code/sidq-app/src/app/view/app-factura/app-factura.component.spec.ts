import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppFacturaComponent } from './app-factura.component';

describe('AppFacturaComponent', () => {
  let component: AppFacturaComponent;
  let fixture: ComponentFixture<AppFacturaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppFacturaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AppFacturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
