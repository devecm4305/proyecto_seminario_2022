import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Factura } from 'src/app/model/factura';
import { CajaServiceService } from 'src/app/service/caja-service.service';
import { ClienteService } from 'src/app/service/cliente.service';
import { FacturaService } from 'src/app/service/factura.service';

import {StepsModule} from 'primeng/steps';
import {MenuItem} from 'primeng/api';
import { MenuItemContent } from 'primeng/menu';
import { PrintFacturaService } from 'src/app/service/data/print-factura.service';
import { DetalleFactura } from 'src/app/model/facturadetalle';
import { ProductoService } from 'src/app/service/producto.service';
import { Table } from 'primeng/table';

@Component({
  selector: 'app-app-factura',
  templateUrl: './app-factura.component.html',
  styleUrls: ['./app-factura.component.scss']
})
export class AppFacturaComponent implements OnInit {

  @ViewChild('dt') table!: Table;

  @ViewChild('filter') filter!: ElementRef;

  loading:boolean = true;

  facturaDialog: boolean = false;
  items!: MenuItem[];

  crearFacturaDialog: boolean = false;

  factura: any;
  nitCliente: any;

  facturaRequest: any;
  facturaEdit!: Factura;
  facturaAnulada: Factura = new Factura();

  facturaForm = this.formBuilder.group({
    autorizacion: ['', [Validators.required]],
    serie: ['', [Validators.required]],
    caja: ['', [Validators.required]],
    formaPago: ['', [Validators.required]],
    usuario: ['', [Validators.required]],
    cliente: ['', [Validators.required]],
    estado: ['', [Validators.required]],
    monto: ['', [Validators.required]]
  });

  visualizarFactura: boolean = false;

  constructor(private formBuilder: FormBuilder,
              private facturaService: FacturaService,
              private clienteService: ClienteService,
              private cajaService: CajaServiceService,
              private productoService: ProductoService,
              private _dataFactura: PrintFacturaService) {
                this.facturaRequest = new Factura();
              }

  ngOnInit(): void {
    this.cargarFacturas();
    this.items = [
      {
        label: 'Step 1',
        routerLink: 'pasoUno'
      },
      {
        label: 'Step 2',
        routerLink: 'pasoDos'
      },
      {label: 'Step 3'}
  ];
  }

  cargarFacturas(): void{
    this.facturaService.getFacturas().subscribe(
      (response) =>{
        this.factura = response;
        for (let i=0; i<this.factura.length;i++){
          this.clienteService.getClienteId(Number(this.factura[i].cliente)).subscribe(
            (response) => {
              this.factura[i].cliente = response.primerNombre.toUpperCase() + ' '+ response.segundoNombre.toUpperCase() + ' ' + response.primerApellido.toUpperCase() + ' ' + response.segundoApellido.toUpperCase();
              this.factura[i].nitCliente = response.nit;
            }
          );
          this.cajaService.getCajaId(Number(this.factura[i].cliente)).subscribe(
            (response) => {
              this.factura[i].caja = response.codigo;
            }
          );
        }
        this.loading = false;
      }
    );
  }

  registrarFactura(){

    this.facturaRequest.autorizacion = String(this.facturaForm.get('autorizacion')?.value);
    this.facturaRequest.serie = String(this.facturaForm.get('serie')?.value);
    this.facturaRequest.caja = String(this.facturaForm.get('caja')?.value);
    this.facturaRequest.formaPago = String(this.facturaForm.get('formaPago')?.value);
    this.facturaRequest.usuario = String(this.facturaForm.get('usuario')?.value);
    this.facturaRequest.cliente = String(this.facturaForm.get('cliente')?.value);
    this.facturaRequest.estado = String(this.facturaForm.get('estado')?.value);
    this.facturaRequest.monto = String(this.facturaForm.get('monto')?.value);

    console.log('  --  '+JSON.stringify(this.facturaRequest));

    this.facturaService.postFactura(this.facturaRequest).subscribe(data => {
      this.facturaForm.setValue({autorizacion: '', serie: '', caja: '', formaPago: '', usuario:'', cliente: '', estado: '', monto:''});
      this.cargarFacturas();
    });
  }

  hideDialog(){
    this.facturaDialog = false;
  }

  editFactura(facturaE: Factura) {
    this.facturaEdit = {...facturaE};
    this.facturaDialog = true;
  }

  hideDialogCrearFactura(){
    this.crearFacturaDialog = false;
  }

  createFactura(){
    this.crearFacturaDialog = true;
  }

  update(){
    this.facturaService.putFactura(this.facturaEdit).subscribe(data =>{
      this.cargarFacturas();
      this.facturaDialog = false;
    });
  }

  anular(fAnulada: Factura){
    this.facturaAnulada = {...fAnulada};
    this.facturaAnulada.estado =0;
    //console.log('',JSON.stringify(facturaAnulada));
    this.facturaService.anularFactura(this.facturaAnulada.id).subscribe(data =>{
      this.cargarFacturas();
    });
  }

  verFactura(id: number){
    this.visualizarFactura = true;
    this.facturaService.getFacturaById(id).subscribe(data =>{
      let factura:any = data;
      let fac: Factura = factura;
      let detalleFac: DetalleFactura[]=[];
      this.facturaService.getDetalleByFactura(id).subscribe(dataDetalle =>{
        let detalle:any = dataDetalle;
        for(let i=0; i < detalle.length; i++) {
          let item = new DetalleFactura;
          item.cantidad=detalle[i].cantidad;
          item.subTotal=detalle[i].subTotal;
          this.productoService.getProducto(detalle[i].producto).subscribe (dataProducto => {
            let producto:any = dataProducto;
            item.codigoProducto = producto.codigoProducto;
            item.producto = producto.nombre
            item.precioUnitario = producto.precioUnitario;
          });

          detalleFac.push(item);
        }
      });
      fac.listDetalle = detalleFac;
      this._dataFactura.setDataStream(fac);
    });
  }

  clear(table: Table) {
    table.clear();
    this.filter.nativeElement.value = '';
  }

}
