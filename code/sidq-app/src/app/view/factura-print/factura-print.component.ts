import { Component, OnInit } from '@angular/core';
import { DetalleFactura } from 'src/app/model/facturadetalle';
import { ClienteService } from 'src/app/service/cliente.service';
import { PrintFacturaService } from 'src/app/service/data/print-factura.service';

@Component({
  selector: 'app-factura-print',
  templateUrl: './factura-print.component.html',
  styleUrls: ['./factura-print.component.scss']
})
export class FacturaPrintComponent implements OnInit {

  fechaFactura = new Date().getTime().toString();

  monto: number = 0;
  nombreCliente: string = '';
  nitCliente: string = '';
  autorizacion: string = '';
  serie: string ='';
  codigoCliente: string = '';
  direccionCliente: string = '';
  itemDetalleFactura: DetalleFactura[]=[];

  constructor(private _dataFactura: PrintFacturaService,
              private _dataCliente: ClienteService) { }

  ngOnInit(): void {
    this._dataFactura.getDataStream().subscribe((dataFactura) =>{
      if(null!=dataFactura){
        this.monto = dataFactura.monto;
        if(dataFactura.cliente!=0){
          this._dataCliente.getClienteId(dataFactura.cliente).subscribe((dataCliente)=>{
            this.nombreCliente = dataCliente.primerNombre + ' ' + dataCliente.segundoNombre + ' ' + dataCliente.primerApellido + ' ' + dataCliente.segundoApellido;
            this.nitCliente = dataCliente.nit;
            this.codigoCliente = dataCliente.codigo;
            this.direccionCliente = dataCliente.direccion;
          });
          this.autorizacion = dataFactura.autorizacion;
          this.serie = dataFactura.serie;
          this.itemDetalleFactura = dataFactura.listDetalle;
        }
      }
    });
  }

  print() {
    window.print();
  }

}
