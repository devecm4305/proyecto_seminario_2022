export class Marca {
  id!: number;
  nombre!: String;
  estado!: number;
  descripcion!: String;
  calificacion!: number;
}
