export class DetalleCompra {
  compra!: number;
  subTotal!: number;
  marca!: string;
  marcaId!: number;
  precioUnitario!: number;
  producto!: number;
  productoId!: number;
  codigoProducto!: string;
  cantidad!: number;
}
