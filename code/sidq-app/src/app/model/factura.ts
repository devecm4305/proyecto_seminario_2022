import { DetalleFactura } from "./facturadetalle";

export class Factura {
  id!: number;
  autorizacion!: string;
  serie!: string;
  caja!: number;
  formaPago!: number;
  usuario!: number;
  cliente!: number;
  estado!: number;
  monto!: number;
  listDetalle!: DetalleFactura[];
}
