export class DetalleFactura {
  id!: number;
  descuento!: number;
  cantidad!:	number;
  factura!:	number;
  producto!:	number;
  descripcion!:	string;
  codigoProducto!: string;
  precioUnitario!: number;
  subTotal!: number;
  productoId!: number;
  marca!: string;
  marcaId!: number;
}
