export class Producto {
  id!: number;
  nombre!: String;
  descripcion!: String;
  estado!: number;
  existencias!: number;
  precioUnitario!: number;
  categoria!: number;
  marca!: number;
  proveedor!: number;
  codigoProducto!: String;
}
