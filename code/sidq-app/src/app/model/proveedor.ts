export class Proveedor {
  id!: number;
  nombre!: String;
  direccion!: String;
  telefonoContacto!: String;
  celularContacto!:  String;
  emailContacto!: String;
  pagina!: String;
  codigo!: String;
  descripcion!: String;
  estado!: number;
}
