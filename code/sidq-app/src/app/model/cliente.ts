export class Cliente {
  id!: number;
  codigo!: string;
  primerNombre!: string;
  segundoNombre!: string;
  primerApellido!: string;
  segundoApellido!: string;
  direccion!: string;
  nit!: string;
  correoElectronico!: string;
}
