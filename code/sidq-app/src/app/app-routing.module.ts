import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { KeycloakAuthGuard } from 'keycloak-angular';
import { AppMainComponent } from './app.main.component';
import { AuthGuard } from './auth/auth.guard';
import { KeycloakGuard } from './keycloak.guard';
import { AppCajasComponent } from './view/app-cajas/app-cajas.component';
import { AppCategoriaComponent } from './view/app-categoria/app-categoria.component';
import { AppClienteComponent } from './view/app-cliente/app-cliente.component';
import { AppCompraComponent } from './view/app-compra/app-compra.component';
import { AppDashboardComponent } from './view/app-dashboard/app-dashboard.component';
import { AppFacturaComponent } from './view/app-factura/app-factura.component';
import { AppFacturaSetpOneComponent } from './view/app-factura/stepOne/app-factura-stepone.component';
import { AppFacturaSetpTwoComponent } from './view/app-factura/stepTwo/app-factura-steptwo.component';
import { AppFormapagoComponent } from './view/app-formapago/app-formapago.component';
import { AppLoginComponent } from './view/app-login/app-login.component';
import { AppMarcaComponent } from './view/app-marca/app-marca.component';
import { AppProductosComponent } from './view/app-productos/app-productos.component';
import { AppProveedorComponent } from './view/app-proveedor/app-proveedor.component';
import { FacturaPrintComponent } from './view/factura-print/factura-print.component';

const routes: Routes = [
  {
    path: '', component:AppMainComponent, canActivate: [KeycloakGuard],
    children: [
      {path: '', component: AppDashboardComponent},
      {path: 'sidq/productos', component: AppProductosComponent},
      {path: 'sidq/cajas', component: AppCajasComponent},
      {path: 'sidq/categorias', component: AppCategoriaComponent},
      {path: 'sidq/marcas', component: AppMarcaComponent},
      {path: 'sidq/proveedores', component: AppProveedorComponent},
      {
        path: 'sidq/facturas', component: AppFacturaComponent,
        children: [
          //{path: '', redirectTo: 'pasoUno', pathMatch: 'full'},

          {path:'pasoDos', component: AppFacturaSetpTwoComponent}
        ]
      },
      {path: 'sidq/clientes', component: AppClienteComponent},
      {path: 'sidq/formasPago', component: AppFormapagoComponent},
      {path: 'sidq/compras', component: AppCompraComponent}
    ]
  },
  {path:'pasoUno', component: AppFacturaSetpOneComponent},
  {path:'sidq/factura-print', component: FacturaPrintComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
