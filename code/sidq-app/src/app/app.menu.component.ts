import { Component, OnInit } from '@angular/core';
import { AppMainComponent } from './app.main.component';

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit {

    model!: any[];

    constructor(public app: AppMainComponent) { }

    ngOnInit() {
        this.model = [
            {label: 'Dashboard', icon: 'pi pi-fw pi-home', routerLink: ['/']},

            {
                label: 'Catálogos', icon: 'pi pi-fw pi-align-left',
                items: [
                  {
                    label: 'Productos', icon: 'pi pi-fw pi-align-left',
                    items: [
                      {label: 'Categoria', icon: 'pi pi-fw pi-align-left', routerLink: ['/sidq/categorias']},
                      {label: 'Marca', icon: 'pi pi-fw pi-align-left', routerLink: ['/sidq/marcas']}
                    ]
                  },
                  {label: 'Proveedores', icon: 'pi pi-fw pi-align-left', routerLink:['/sidq/proveedores']},
                  {label: 'Cajas', icon: 'pi pi-fw pi-align-left', routerLink:['/sidq/cajas'],},
                  {label: 'Formas de Pago', icon: 'pi pi-fw pi-align-left', routerLink: ['/sidq/formasPago']}
                ]
            },
            {label: 'Inventario', icon: 'pi pi-fw pi-align-left', routerLink: ['/sidq/productos']},
            {label: 'Factura', icon: 'pi pi-fw pi-align-left', routerLink: ['/sidq/facturas']},
            {label: 'Cliente', icon: 'pi pi-fw pi-align-left', routerLink: ['/sidq/clientes']},
            {label: 'Compras', icon: 'pi pi-fw pi-align-left', routerLink: ['/sidq/compras']}
        ];
    }

    onMenuClick(event: any) {
        this.app.onMenuClick(event);
    }
}
