import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Factura } from 'src/app/model/factura';
import { DetalleFactura } from 'src/app/model/facturadetalle';

@Injectable({
  providedIn: 'root'
})
export class PrintFacturaService {

  private _dataStream = new BehaviorSubject<Factura>({
    id: 0,
    autorizacion: "",
    serie: "",
    caja: 0,
    formaPago: 0,
    usuario: 0,
    cliente: 0,
    estado: 0,
    monto: 0,
    listDetalle: []
  });

  constructor() { }

  getDataStream(): Observable<Factura>{
    return this._dataStream;
  }

  setDataStream(valueFactura: Factura) {
    return this._dataStream.next(valueFactura);
  }

}
