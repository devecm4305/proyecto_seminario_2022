import { TestBed } from '@angular/core/testing';

import { PrintFacturaService } from './print-factura.service';

describe('PrintFacturaService', () => {
  let service: PrintFacturaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PrintFacturaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
