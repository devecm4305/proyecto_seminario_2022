import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FormaPago } from '../model/formapago';

@Injectable({
  providedIn: 'root'
})
export class FormapagoService {

  private url = 'http://localhost:8081/v1.0/formasPago';


  constructor(private http: HttpClient) { }

  getFormasPago(){
    return this.http.get(this.url);
  }

  getFormaPagoId(id: number): Observable<any> {
    return this.http.get(this.url+'/'+id);
  }

  postFormaPago(formaPago: FormaPago): Observable<any>{
    const headers = {'content-type': 'application/json'};
    const body = JSON.stringify(formaPago);
    return this.http.post(this.url,body,{headers: headers});
  }

  putFormaPago(formaPago: FormaPago): Observable<any>{
    const headers = {'content-type': 'application/json'};
    const body = JSON.stringify(formaPago);
    return this.http.put(this.url,body,{headers: headers});
  }

}
