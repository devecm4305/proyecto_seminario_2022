import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Producto } from '../model/producto';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  private url ='http://localhost:8087/v1.0/productos'

  constructor(private http: HttpClient) { }

  getProductos(){
    return this.http.get(this.url);
  }

  getProductosExistencias(){
    return this.http.get(this.url+'/getExistencias');
  }

  getProductoByCodigoProducto(codigoProducto: string){
    const params = new HttpParams()
    .set('codigoProducto', codigoProducto);
    return this.http.get(this.url+'/findByCodigoProducto')
  }

  getProducto(id: number): Observable<any>{
    return this.http.get(this.url+'/'+id);
  }

  postProducto(producto: Producto): Observable<any>{
    const headers = {'content-type': 'application/json'}
    const body = JSON.stringify(producto);
    return this.http.post(this.url,body,{headers: headers})
  }

  putProducto(producto: Producto): Observable<any>{
    const headers = {'content-type': 'application/json'}
    const body = JSON.stringify(producto);
    return this.http.put(this.url,body,{headers: headers})
  }

}
