import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Marca } from '../model/marca';

@Injectable({
  providedIn: 'root'
})
export class MarcaService {

  private url = 'http://localhost:8083/v1.0/marcas';

  constructor(private http: HttpClient) { }

  getMarcas(){
    return this.http.get(this.url);
  }

  getMarca(id: number): Observable<any>{
    return this.http.get(this.url+'/'+id);
  }

  postMarcas(marca: Marca): Observable<any>{
    const headers = {'content-type': 'application/json'}
    const body = JSON.stringify(marca);
    return this.http.post(this.url,body, {headers:headers});
  }

}
