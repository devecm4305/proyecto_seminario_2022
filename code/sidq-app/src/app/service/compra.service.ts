import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Compra } from '../model/compra';
import { DetalleCompra } from '../model/detallecompra';

@Injectable({
  providedIn: 'root'
})
export class CompraService {

  private url = 'http://localhost:8088/v1.0/compras';
  private urlDet = 'http://localhost:8088/v1.0/detalleCompras';

  constructor(private http: HttpClient) { }

  getCompras(){
    return this.http.get(this.url);
  }

  getDetalleByIdCompra(idCompra: number){
    const params = new HttpParams()
    .set('idCompra', idCompra);
    return this.http.get(this.urlDet+'/findByIdCompra',{params});
  }

  postCompra(compra: Compra): Observable<any>{
    const headers = {'content-type': 'application/json'}
    const body = JSON.stringify(compra);
    return this.http.post(this.url,body,{headers:headers});
  }

  postDetalleCompra(detalleCompra: DetalleCompra): Observable<any>{
    const headers = {'content-type': 'application/json'}
    const body = JSON.stringify(detalleCompra);
    return this.http.post(this.urlDet,body,{headers:headers});
  }


  putCompra(compra: Compra): Observable<any>{
    const headers = {'content-type': 'application/json'}
    const body = JSON.stringify(compra);
    return this.http.put(this.url,body,{headers:headers});
  }

}
