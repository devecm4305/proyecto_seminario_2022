import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Categoria } from '../model/categoria';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  private url = 'http://localhost:8081/v1.0/categorias';

  constructor(private http: HttpClient) { }

  getCategoria(){
    return this.http.get(this.url);
  }

  getCategoriaId(id: number): Observable<any> {
    return this.http.get(this.url+'/'+id);
  }

  postCategoria(categoria: Categoria): Observable<any>{
    const headers = {'content-type': 'application/json'};
    const body = JSON.stringify(categoria);
    return this.http.post(this.url,body,{headers: headers});
  }

  putCategoria(categoria: Categoria): Observable<any>{
    const headers = {'content-type': 'application/json'};
    const body = JSON.stringify(categoria);
    return this.http.put(this.url,body,{headers: headers});
  }

}
