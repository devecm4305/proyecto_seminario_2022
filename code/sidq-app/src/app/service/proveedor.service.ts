import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Proveedor } from '../model/proveedor';

@Injectable({
  providedIn: 'root'
})
export class ProveedorService {

  private url = 'http://localhost:8082/v1.0/proveedores'

  constructor(private http: HttpClient) { }

  getProveedores(){
    return this.http.get(this.url);
  }

  getProveedor(id: number): Observable<any>{
    return this.http.get(this.url+'/'+id);
  }

  postProveedor(proveedor: Proveedor): Observable<any> {
    const headers = {'content-type': 'application/json'}
    const body = JSON.stringify(proveedor);
    return this.http.post(this.url,body,{headers: headers})
  }

}
