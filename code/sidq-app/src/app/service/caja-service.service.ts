import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Caja } from '../model/caja';

@Injectable({
  providedIn: 'root'
})
export class CajaServiceService {

  private url = 'http://localhost:8081/v1.0/cajas';

  constructor(private http: HttpClient) { }

  getCajas(){
    return this.http.get(this.url);
  }

  getCajaId(id: number): Observable<any> {
    return this.http.get(this.url+'/'+id);
  }

  postCajas(caja: Caja): Observable<any>{
    const headers = {'content-type': 'application/json'}
    const body = JSON.stringify(caja);
    return this.http.post(this.url,body,{headers: headers})
  }

  putCajas(caja: Caja): Observable<any>{
    const headers = {'content-type': 'application/json'};
    const body = JSON.stringify(caja);
    return this.http.put(this.url,body,{headers:headers});
  }


}
