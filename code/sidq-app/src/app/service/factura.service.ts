import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Factura } from '../model/factura';
import { DetalleFactura } from '../model/facturadetalle';

@Injectable({
  providedIn: 'root'
})
export class FacturaService {

  private url = 'http://localhost:8084/v1.0/facturas';
  private urlDet = 'http://localhost:8084/v1.0/facturasDetalles';

  constructor(private http: HttpClient) { }

  getFacturas(){
    return this.http.get(this.url);
  }

  getProductosMes(){
    return this.http.get(this.urlDet+'/findProductosByMes');
  }

  getFacturaById(id: number){
    return this.http.get(this.url+'/'+id);
  }

  getDetalleByFactura(id: number){
    const params = new HttpParams()
    .set('factura', id);
    return this.http.get(this.urlDet+'/findByFactura',{params});
  }

  postDetalleFactura(detalleFactura: DetalleFactura): Observable<any>{
    const headers = {'content-type': 'application/json'}
    const body = JSON.stringify(detalleFactura);
    return this.http.post(this.urlDet,body, {headers:headers});
  }

  postFactura(factura: Factura): Observable<any>{
    const headers = {'content-type': 'application/json'}
    const body = JSON.stringify(factura);
    return this.http.post(this.url,body, {headers:headers});
  }

  putFactura(factura: Factura): Observable<any>{
    const headers = {'content-type': 'application/json'}
    const body = JSON.stringify(factura);
    return this.http.put(this.url,body, {headers:headers});
  }

  anularFactura(idFactura: number): Observable<any>{
    const params = new HttpParams()
    .set('idFactura', idFactura);
    return this.http.delete(this.url+'/anular',{params});
  }
}
