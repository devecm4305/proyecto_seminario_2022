import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Cliente } from '../model/cliente';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  private url ='http://localhost:8085/v1.0/clientes'

  constructor(private http: HttpClient) { }

  getClientes(){
    return this.http.get(this.url);
  }

  getClienteId(id: number): Observable<any> {
    return this.http.get(this.url+'/'+id);
  }

  getClienteByNIT(nit: string): Observable<any>{
    const params = new HttpParams()
    .set('nit', nit);
    return this.http.get(this.url+'/findByNit',{params});
  }

  getClienteByCodigo(codigo: string): Observable<any>{
    const params = new HttpParams()
    .set('codigo', codigo);
    return this.http.get(this.url+'/findByCodigo',{params});
  }

  getClienteByNombreApellido(nombre: string, apellido: string): Observable<any>{
    const  params = new HttpParams()
    .set('primerNombre', nombre)
    .set('primerApellido', apellido);
    return this.http.get(this.url+'/findByNombreApellido',{params});
  }

  postCliente(cliente: Cliente): Observable<any>{
    const headers = {'content-type': 'application/json'}
    const body = JSON.stringify(cliente);
    return this.http.post(this.url,body, {headers:headers});
  }

  putCliente(cliente: Cliente): Observable<any>{
    const headers = {'content-type': 'application/json'}
    const body = JSON.stringify(cliente);
    return this.http.put(this.url,body, {headers:headers});
  }
}
