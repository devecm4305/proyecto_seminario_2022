package com.inerv.gt.sidq.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inerv.gt.sidq.entity.TipoUsuario;

/**
 * 
 */
public interface TipoUsuarioRepository extends JpaRepository<TipoUsuario, Integer> {
    
}
