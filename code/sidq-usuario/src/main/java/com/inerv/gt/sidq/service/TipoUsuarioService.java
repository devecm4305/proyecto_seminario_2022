package com.inerv.gt.sidq.service;

import com.inerv.gt.sidq.entity.TipoUsuario;

/**
 * 
 */
public interface TipoUsuarioService extends SidQServiceGeneric<TipoUsuario>{
    
}
