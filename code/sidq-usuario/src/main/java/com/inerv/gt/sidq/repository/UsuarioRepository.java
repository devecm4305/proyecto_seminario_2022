package com.inerv.gt.sidq.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inerv.gt.sidq.entity.Usuario;

/**
 * 
 * @author ervin
 *
 */
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

}
