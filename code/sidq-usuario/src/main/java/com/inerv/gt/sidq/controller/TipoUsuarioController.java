package com.inerv.gt.sidq.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inerv.gt.sidq.entity.TipoUsuario;
import com.inerv.gt.sidq.service.TipoUsuarioService;

/**
 * 
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/v1.0/tiposUsuarios")
public class TipoUsuarioController extends SidQControllerGeneric<TipoUsuario, TipoUsuarioService> {
    
}
