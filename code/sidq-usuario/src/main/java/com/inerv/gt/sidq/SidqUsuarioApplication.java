package com.inerv.gt.sidq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SidqUsuarioApplication {

	public static void main(String[] args) {
		SpringApplication.run(SidqUsuarioApplication.class, args);
	}

}
