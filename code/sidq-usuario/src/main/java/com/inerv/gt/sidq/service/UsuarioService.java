package com.inerv.gt.sidq.service;

import com.inerv.gt.sidq.entity.Usuario;

/**
 * 
 * @author ervin
 *
 */
public interface UsuarioService extends SidQServiceGeneric<Usuario> {

}
