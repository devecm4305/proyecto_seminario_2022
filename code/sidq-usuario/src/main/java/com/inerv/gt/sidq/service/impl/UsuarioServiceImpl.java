package com.inerv.gt.sidq.service.impl;

import org.springframework.stereotype.Service;

import com.inerv.gt.sidq.entity.Usuario;
import com.inerv.gt.sidq.repository.UsuarioRepository;
import com.inerv.gt.sidq.service.SidQServiceGenericImpl;

/**
 * 
 * @author ervin
 *
 */
@Service
public class UsuarioServiceImpl extends SidQServiceGenericImpl<Usuario, UsuarioRepository>{

}
