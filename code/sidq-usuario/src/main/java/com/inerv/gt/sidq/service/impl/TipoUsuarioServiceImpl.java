package com.inerv.gt.sidq.service.impl;

import org.springframework.stereotype.Service;

import com.inerv.gt.sidq.entity.TipoUsuario;
import com.inerv.gt.sidq.repository.TipoUsuarioRepository;
import com.inerv.gt.sidq.service.SidQServiceGenericImpl;
import com.inerv.gt.sidq.service.TipoUsuarioService;

/**
 * 
 */
@Service
public class TipoUsuarioServiceImpl extends SidQServiceGenericImpl<TipoUsuario, TipoUsuarioRepository> implements TipoUsuarioService{
    
}
