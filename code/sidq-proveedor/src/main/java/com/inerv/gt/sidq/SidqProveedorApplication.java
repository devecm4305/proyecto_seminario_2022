package com.inerv.gt.sidq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SidqProveedorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SidqProveedorApplication.class, args);
	}

}
