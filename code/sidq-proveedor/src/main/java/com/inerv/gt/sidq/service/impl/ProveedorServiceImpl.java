package com.inerv.gt.sidq.service.impl;

import org.springframework.stereotype.Service;

import com.inerv.gt.sidq.entity.Proveedor;
import com.inerv.gt.sidq.repository.ProveedorRepository;
import com.inerv.gt.sidq.service.ProveedorService;
import com.inerv.gt.sidq.service.SidQServiceGenericImpl;

/**
 * @author ecanuz
 */
@Service
public class ProveedorServiceImpl extends SidQServiceGenericImpl<Proveedor, ProveedorRepository> implements ProveedorService{
    
}
