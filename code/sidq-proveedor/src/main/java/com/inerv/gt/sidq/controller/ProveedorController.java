package com.inerv.gt.sidq.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inerv.gt.sidq.entity.Proveedor;
import com.inerv.gt.sidq.service.ProveedorService;

/**
 * @author ecanuz
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/v1.0/proveedores")
public class ProveedorController extends SidQControllerGeneric<Proveedor, ProveedorService>{

}
