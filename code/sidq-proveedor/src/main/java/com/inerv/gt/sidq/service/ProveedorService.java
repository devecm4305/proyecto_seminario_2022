package com.inerv.gt.sidq.service;

import com.inerv.gt.sidq.entity.Proveedor;

/**
 * @author ecanuz
 */
public interface ProveedorService extends SidQServiceGeneric<Proveedor> {
    
}
