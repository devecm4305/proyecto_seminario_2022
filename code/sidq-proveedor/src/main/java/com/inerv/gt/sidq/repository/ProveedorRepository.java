package com.inerv.gt.sidq.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inerv.gt.sidq.entity.Proveedor;

/**
 * 
 */
public interface ProveedorRepository extends JpaRepository<Proveedor, Integer> {

}