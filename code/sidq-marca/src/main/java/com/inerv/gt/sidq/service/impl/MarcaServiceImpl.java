package com.inerv.gt.sidq.service.impl;

import org.springframework.stereotype.Service;

import com.inerv.gt.sidq.entity.Marca;
import com.inerv.gt.sidq.repository.MarcaRepository;
import com.inerv.gt.sidq.service.MarcaService;
import com.inerv.gt.sidq.service.SidQServiceGenericImpl;

/**
 * @author ecanuz
 */
@Service
public class MarcaServiceImpl extends SidQServiceGenericImpl<Marca, MarcaRepository> implements MarcaService {
    
}
