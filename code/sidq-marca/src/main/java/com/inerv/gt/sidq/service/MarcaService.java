package com.inerv.gt.sidq.service;

import com.inerv.gt.sidq.entity.Marca;

/**
 * @author ecanuz
 */
public interface MarcaService extends SidQServiceGeneric<Marca>{
    
}
