package com.inerv.gt.sidq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SidqMarcaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SidqMarcaApplication.class, args);
	}

}
