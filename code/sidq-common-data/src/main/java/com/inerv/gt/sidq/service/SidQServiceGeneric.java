package com.inerv.gt.sidq.service;

import java.util.Optional;

/**
 * 
 * @author ecanuz
 *
 */
public interface SidQServiceGeneric<E> {

	public Iterable<E> findAll();
	public Optional<E> findById(Integer id);
	public E save(E entity);
	public void update(E entity);
	public void deleteById(Integer id);
	
}
