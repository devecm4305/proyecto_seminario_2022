package com.inerv.gt.sidq.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inerv.gt.sidq.entity.Caja;
import com.inerv.gt.sidq.service.CajaService;

/**
 * 
 * @author ecanuz
 *
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/v1.0/cajas")
public class CajaController extends SidQControllerGeneric<Caja, CajaService>{

}
