package com.inerv.gt.sidq.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inerv.gt.sidq.entity.FormaPago;

/**
 * @author ecanuz
 */
public interface FormaPagoRepository extends JpaRepository<FormaPago, Integer>{
    
}
