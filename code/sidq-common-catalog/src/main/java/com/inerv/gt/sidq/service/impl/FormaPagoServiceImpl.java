package com.inerv.gt.sidq.service.impl;

import org.springframework.stereotype.Service;

import com.inerv.gt.sidq.entity.FormaPago;
import com.inerv.gt.sidq.repository.FormaPagoRepository;
import com.inerv.gt.sidq.service.FormaPagoService;
import com.inerv.gt.sidq.service.SidQServiceGenericImpl;

/**
 * @author ecanuz
 */
@Service
public class FormaPagoServiceImpl extends SidQServiceGenericImpl<FormaPago, FormaPagoRepository> implements FormaPagoService{
    
}
