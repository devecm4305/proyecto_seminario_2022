package com.inerv.gt.sidq.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inerv.gt.sidq.entity.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Integer>{
    
}
