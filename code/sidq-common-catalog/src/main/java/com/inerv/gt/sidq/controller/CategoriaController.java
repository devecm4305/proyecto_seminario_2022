package com.inerv.gt.sidq.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inerv.gt.sidq.entity.Categoria;
import com.inerv.gt.sidq.service.CategoriaService;

/**
 * @author ervin
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/v1.0/categorias")
public class CategoriaController extends SidQControllerGeneric<Categoria, CategoriaService>{
    
}
