package com.inerv.gt.sidq.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inerv.gt.sidq.entity.Marca;
import com.inerv.gt.sidq.service.MarcaService;

/**
 * @author ecanuz
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/v1.0/marcas")
public class MarcaController extends SidQControllerGeneric<Marca, MarcaService>{
    
}
