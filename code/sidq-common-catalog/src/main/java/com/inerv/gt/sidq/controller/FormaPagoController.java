package com.inerv.gt.sidq.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inerv.gt.sidq.entity.FormaPago;
import com.inerv.gt.sidq.service.FormaPagoService;

/**
 * @author ecanuz
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/v1.0/formasPago")
public class FormaPagoController extends SidQControllerGeneric<FormaPago, FormaPagoService>{
    
}
