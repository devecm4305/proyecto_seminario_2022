package com.inerv.gt.sidq.service.impl;

import org.springframework.stereotype.Service;

import com.inerv.gt.sidq.entity.Caja;
import com.inerv.gt.sidq.repository.CajaRepository;
import com.inerv.gt.sidq.service.CajaService;
import com.inerv.gt.sidq.service.SidQServiceGenericImpl;

/**
 * 
 * @author ecanuz
 *
 */
@Service
public class CajaServiceImpl extends SidQServiceGenericImpl<Caja, CajaRepository> implements CajaService {

}
