package com.inerv.gt.sidq.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inerv.gt.sidq.entity.Marca;

/**
 * @author ecanuz
 */
public interface MarcaRepository extends JpaRepository<Marca, Integer>{
    
}
