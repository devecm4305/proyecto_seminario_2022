package com.inerv.gt.sidq.service;

import com.inerv.gt.sidq.entity.Categoria;

/**
 * @author ervin
 */
public interface CategoriaService extends SidQServiceGeneric<Categoria>{
    
}
