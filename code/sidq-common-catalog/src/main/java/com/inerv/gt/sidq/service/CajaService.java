package com.inerv.gt.sidq.service;

import com.inerv.gt.sidq.entity.Caja;

/**
 * 
 * @author ecanuz
 *
 */
public interface CajaService extends SidQServiceGeneric<Caja> {

}
