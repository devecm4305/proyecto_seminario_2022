package com.inerv.gt.sidq.service.impl;

import org.springframework.stereotype.Service;

import com.inerv.gt.sidq.entity.Categoria;
import com.inerv.gt.sidq.repository.CategoriaRepository;
import com.inerv.gt.sidq.service.CategoriaService;
import com.inerv.gt.sidq.service.SidQServiceGenericImpl;


/**
 * @author ervin
 */
@Service
public class CategoriaServiceImpl extends SidQServiceGenericImpl<Categoria, CategoriaRepository> implements CategoriaService {
    
}
