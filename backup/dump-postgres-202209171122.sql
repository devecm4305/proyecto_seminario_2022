--
-- PostgreSQL database dump
--

-- Dumped from database version 14.3
-- Dumped by pg_dump version 14.2

-- Started on 2022-09-17 11:22:28

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE postgres;
--
-- TOC entry 4477 (class 1262 OID 14672)
-- Name: postgres; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.UTF-8';


ALTER DATABASE postgres OWNER TO postgres;

\connect postgres

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4478 (class 0 OID 0)
-- Dependencies: 4477
-- Name: DATABASE postgres; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- TOC entry 5 (class 2615 OID 16427)
-- Name: sidq; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sidq;


ALTER SCHEMA sidq OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 216 (class 1259 OID 16443)
-- Name: caja; Type: TABLE; Schema: sidq; Owner: postgres
--

CREATE TABLE sidq.caja (
    id integer NOT NULL,
    codigo integer NOT NULL,
    estado integer NOT NULL
);


ALTER TABLE sidq.caja OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16442)
-- Name: caja_id_seq; Type: SEQUENCE; Schema: sidq; Owner: postgres
--

CREATE SEQUENCE sidq.caja_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sidq.caja_id_seq OWNER TO postgres;

--
-- TOC entry 4479 (class 0 OID 0)
-- Dependencies: 215
-- Name: caja_id_seq; Type: SEQUENCE OWNED BY; Schema: sidq; Owner: postgres
--

ALTER SEQUENCE sidq.caja_id_seq OWNED BY sidq.caja.id;


--
-- TOC entry 218 (class 1259 OID 16450)
-- Name: categoria; Type: TABLE; Schema: sidq; Owner: postgres
--

CREATE TABLE sidq.categoria (
    id integer NOT NULL,
    nombre character varying(64) NOT NULL,
    descripcion character varying(128)
);


ALTER TABLE sidq.categoria OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16449)
-- Name: categoria_id_seq; Type: SEQUENCE; Schema: sidq; Owner: postgres
--

CREATE SEQUENCE sidq.categoria_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sidq.categoria_id_seq OWNER TO postgres;

--
-- TOC entry 4480 (class 0 OID 0)
-- Dependencies: 217
-- Name: categoria_id_seq; Type: SEQUENCE OWNED BY; Schema: sidq; Owner: postgres
--

ALTER SEQUENCE sidq.categoria_id_seq OWNED BY sidq.categoria.id;


--
-- TOC entry 234 (class 1259 OID 16506)
-- Name: cliente; Type: TABLE; Schema: sidq; Owner: postgres
--

CREATE TABLE sidq.cliente (
    id integer NOT NULL,
    codigo character varying(16) NOT NULL,
    primer_nombre character varying(32) NOT NULL,
    segundo_nombre character varying(32),
    primer_apellido character varying(32) NOT NULL,
    segundo_apellido character varying(32),
    direccion character varying(128) NOT NULL,
    nit character varying(16) NOT NULL,
    correo_electronico character varying(64) NOT NULL
);


ALTER TABLE sidq.cliente OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 16505)
-- Name: cliente_id_seq; Type: SEQUENCE; Schema: sidq; Owner: postgres
--

CREATE SEQUENCE sidq.cliente_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sidq.cliente_id_seq OWNER TO postgres;

--
-- TOC entry 4481 (class 0 OID 0)
-- Dependencies: 233
-- Name: cliente_id_seq; Type: SEQUENCE OWNED BY; Schema: sidq; Owner: postgres
--

ALTER SEQUENCE sidq.cliente_id_seq OWNED BY sidq.cliente.id;


--
-- TOC entry 226 (class 1259 OID 16478)
-- Name: compra; Type: TABLE; Schema: sidq; Owner: postgres
--

CREATE TABLE sidq.compra (
    id integer NOT NULL,
    codigo character varying(32) NOT NULL,
    fecha_creacion date NOT NULL,
    total numeric(4,2) NOT NULL,
    forma_pago integer NOT NULL,
    usuario integer NOT NULL
);


ALTER TABLE sidq.compra OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 16477)
-- Name: compra_id_seq; Type: SEQUENCE; Schema: sidq; Owner: postgres
--

CREATE SEQUENCE sidq.compra_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sidq.compra_id_seq OWNER TO postgres;

--
-- TOC entry 4482 (class 0 OID 0)
-- Dependencies: 225
-- Name: compra_id_seq; Type: SEQUENCE OWNED BY; Schema: sidq; Owner: postgres
--

ALTER SEQUENCE sidq.compra_id_seq OWNED BY sidq.compra.id;


--
-- TOC entry 228 (class 1259 OID 16485)
-- Name: detalle_compra; Type: TABLE; Schema: sidq; Owner: postgres
--

CREATE TABLE sidq.detalle_compra (
    id integer NOT NULL,
    compra integer NOT NULL,
    producto integer NOT NULL,
    precio_unitario numeric(4,2) NOT NULL
);


ALTER TABLE sidq.detalle_compra OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 16484)
-- Name: detalle_compra_id_seq; Type: SEQUENCE; Schema: sidq; Owner: postgres
--

CREATE SEQUENCE sidq.detalle_compra_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sidq.detalle_compra_id_seq OWNER TO postgres;

--
-- TOC entry 4483 (class 0 OID 0)
-- Dependencies: 227
-- Name: detalle_compra_id_seq; Type: SEQUENCE OWNED BY; Schema: sidq; Owner: postgres
--

ALTER SEQUENCE sidq.detalle_compra_id_seq OWNED BY sidq.detalle_compra.id;


--
-- TOC entry 236 (class 1259 OID 16513)
-- Name: detalle_factura; Type: TABLE; Schema: sidq; Owner: postgres
--

CREATE TABLE sidq.detalle_factura (
    id integer NOT NULL,
    descuento numeric(4,2) NOT NULL,
    cantidad integer NOT NULL,
    factura integer NOT NULL,
    producto integer NOT NULL,
    descripcion character varying(128)
);


ALTER TABLE sidq.detalle_factura OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 16512)
-- Name: detalle_factura_id_seq; Type: SEQUENCE; Schema: sidq; Owner: postgres
--

CREATE SEQUENCE sidq.detalle_factura_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sidq.detalle_factura_id_seq OWNER TO postgres;

--
-- TOC entry 4484 (class 0 OID 0)
-- Dependencies: 235
-- Name: detalle_factura_id_seq; Type: SEQUENCE OWNED BY; Schema: sidq; Owner: postgres
--

ALTER SEQUENCE sidq.detalle_factura_id_seq OWNED BY sidq.detalle_factura.id;


--
-- TOC entry 232 (class 1259 OID 16499)
-- Name: factura; Type: TABLE; Schema: sidq; Owner: postgres
--

CREATE TABLE sidq.factura (
    id integer NOT NULL,
    fecha_creacion date NOT NULL,
    autorizacion character varying(128) NOT NULL,
    fecha_emision date NOT NULL,
    fecha_certificacion date NOT NULL,
    serie character varying(32) NOT NULL,
    caja integer NOT NULL,
    forma_pago integer NOT NULL,
    usuario integer NOT NULL,
    cliente integer NOT NULL,
    estado integer NOT NULL
);


ALTER TABLE sidq.factura OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 16498)
-- Name: factura_id_seq; Type: SEQUENCE; Schema: sidq; Owner: postgres
--

CREATE SEQUENCE sidq.factura_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sidq.factura_id_seq OWNER TO postgres;

--
-- TOC entry 4485 (class 0 OID 0)
-- Dependencies: 231
-- Name: factura_id_seq; Type: SEQUENCE OWNED BY; Schema: sidq; Owner: postgres
--

ALTER SEQUENCE sidq.factura_id_seq OWNED BY sidq.factura.id;


--
-- TOC entry 230 (class 1259 OID 16492)
-- Name: forma_pago; Type: TABLE; Schema: sidq; Owner: postgres
--

CREATE TABLE sidq.forma_pago (
    id integer NOT NULL,
    nombre character varying(64) NOT NULL,
    decripcion character varying(128) NOT NULL,
    estado integer NOT NULL,
    descripcion character varying(255)
);


ALTER TABLE sidq.forma_pago OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 16491)
-- Name: forma_pago_id_seq; Type: SEQUENCE; Schema: sidq; Owner: postgres
--

CREATE SEQUENCE sidq.forma_pago_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sidq.forma_pago_id_seq OWNER TO postgres;

--
-- TOC entry 4486 (class 0 OID 0)
-- Dependencies: 229
-- Name: forma_pago_id_seq; Type: SEQUENCE OWNED BY; Schema: sidq; Owner: postgres
--

ALTER SEQUENCE sidq.forma_pago_id_seq OWNED BY sidq.forma_pago.id;


--
-- TOC entry 222 (class 1259 OID 16464)
-- Name: marca; Type: TABLE; Schema: sidq; Owner: postgres
--

CREATE TABLE sidq.marca (
    id integer NOT NULL,
    nombre character varying(32) NOT NULL,
    estado integer NOT NULL,
    fecha_creacion timestamp without time zone NOT NULL,
    descripcion character varying(128),
    calificacion integer NOT NULL
);


ALTER TABLE sidq.marca OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 16463)
-- Name: marca_id_seq; Type: SEQUENCE; Schema: sidq; Owner: postgres
--

CREATE SEQUENCE sidq.marca_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sidq.marca_id_seq OWNER TO postgres;

--
-- TOC entry 4487 (class 0 OID 0)
-- Dependencies: 221
-- Name: marca_id_seq; Type: SEQUENCE OWNED BY; Schema: sidq; Owner: postgres
--

ALTER SEQUENCE sidq.marca_id_seq OWNED BY sidq.marca.id;


--
-- TOC entry 220 (class 1259 OID 16457)
-- Name: producto; Type: TABLE; Schema: sidq; Owner: postgres
--

CREATE TABLE sidq.producto (
    id integer NOT NULL,
    nombre character varying(64) NOT NULL,
    descripcion character varying(128) NOT NULL,
    estado integer NOT NULL,
    existencias integer NOT NULL,
    precio_unitario numeric(4,2) NOT NULL,
    categoria integer NOT NULL,
    marca integer NOT NULL,
    proveedor integer NOT NULL,
    preciounitario integer
);


ALTER TABLE sidq.producto OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 16456)
-- Name: producto_id_seq; Type: SEQUENCE; Schema: sidq; Owner: postgres
--

CREATE SEQUENCE sidq.producto_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sidq.producto_id_seq OWNER TO postgres;

--
-- TOC entry 4488 (class 0 OID 0)
-- Dependencies: 219
-- Name: producto_id_seq; Type: SEQUENCE OWNED BY; Schema: sidq; Owner: postgres
--

ALTER SEQUENCE sidq.producto_id_seq OWNED BY sidq.producto.id;


--
-- TOC entry 224 (class 1259 OID 16471)
-- Name: proveedor; Type: TABLE; Schema: sidq; Owner: postgres
--

CREATE TABLE sidq.proveedor (
    id integer NOT NULL,
    nombre character varying(128) NOT NULL,
    direccion character varying(128) NOT NULL,
    telefono_contacto character varying(16) NOT NULL,
    celular_contacto character varying(16) NOT NULL,
    email_contacto character varying(32) NOT NULL,
    pagina character varying(128) NOT NULL,
    codigo character varying(16) NOT NULL,
    descripcion character varying(255),
    estado integer
);


ALTER TABLE sidq.proveedor OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 16470)
-- Name: proveedor_id_seq; Type: SEQUENCE; Schema: sidq; Owner: postgres
--

CREATE SEQUENCE sidq.proveedor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sidq.proveedor_id_seq OWNER TO postgres;

--
-- TOC entry 4489 (class 0 OID 0)
-- Dependencies: 223
-- Name: proveedor_id_seq; Type: SEQUENCE OWNED BY; Schema: sidq; Owner: postgres
--

ALTER SEQUENCE sidq.proveedor_id_seq OWNED BY sidq.proveedor.id;


--
-- TOC entry 212 (class 1259 OID 16429)
-- Name: tipo_usuario; Type: TABLE; Schema: sidq; Owner: postgres
--

CREATE TABLE sidq.tipo_usuario (
    id integer NOT NULL,
    nombre character varying(32) NOT NULL,
    estado integer NOT NULL,
    descripcion character varying(128) NOT NULL
);


ALTER TABLE sidq.tipo_usuario OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16428)
-- Name: tipo_usuario_id_seq; Type: SEQUENCE; Schema: sidq; Owner: postgres
--

CREATE SEQUENCE sidq.tipo_usuario_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sidq.tipo_usuario_id_seq OWNER TO postgres;

--
-- TOC entry 4490 (class 0 OID 0)
-- Dependencies: 211
-- Name: tipo_usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: sidq; Owner: postgres
--

ALTER SEQUENCE sidq.tipo_usuario_id_seq OWNED BY sidq.tipo_usuario.id;


--
-- TOC entry 214 (class 1259 OID 16436)
-- Name: usuario; Type: TABLE; Schema: sidq; Owner: postgres
--

CREATE TABLE sidq.usuario (
    id integer NOT NULL,
    codigo_empleado character varying(8) NOT NULL,
    cui numeric(13,0) NOT NULL,
    nit character varying(16),
    primer_nombre character varying(32) NOT NULL,
    segndo_nombre character varying(32),
    primer_apellido character varying(32) NOT NULL,
    segundo_apellido character varying(32),
    fecha_creacion date NOT NULL,
    estado integer NOT NULL,
    tipo_usuario integer NOT NULL
);


ALTER TABLE sidq.usuario OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16435)
-- Name: usuario_id_seq; Type: SEQUENCE; Schema: sidq; Owner: postgres
--

CREATE SEQUENCE sidq.usuario_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sidq.usuario_id_seq OWNER TO postgres;

--
-- TOC entry 4491 (class 0 OID 0)
-- Dependencies: 213
-- Name: usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: sidq; Owner: postgres
--

ALTER SEQUENCE sidq.usuario_id_seq OWNED BY sidq.usuario.id;


--
-- TOC entry 4243 (class 2604 OID 16446)
-- Name: caja id; Type: DEFAULT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.caja ALTER COLUMN id SET DEFAULT nextval('sidq.caja_id_seq'::regclass);


--
-- TOC entry 4244 (class 2604 OID 16453)
-- Name: categoria id; Type: DEFAULT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.categoria ALTER COLUMN id SET DEFAULT nextval('sidq.categoria_id_seq'::regclass);


--
-- TOC entry 4252 (class 2604 OID 16509)
-- Name: cliente id; Type: DEFAULT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.cliente ALTER COLUMN id SET DEFAULT nextval('sidq.cliente_id_seq'::regclass);


--
-- TOC entry 4248 (class 2604 OID 16481)
-- Name: compra id; Type: DEFAULT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.compra ALTER COLUMN id SET DEFAULT nextval('sidq.compra_id_seq'::regclass);


--
-- TOC entry 4249 (class 2604 OID 16488)
-- Name: detalle_compra id; Type: DEFAULT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.detalle_compra ALTER COLUMN id SET DEFAULT nextval('sidq.detalle_compra_id_seq'::regclass);


--
-- TOC entry 4253 (class 2604 OID 16516)
-- Name: detalle_factura id; Type: DEFAULT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.detalle_factura ALTER COLUMN id SET DEFAULT nextval('sidq.detalle_factura_id_seq'::regclass);


--
-- TOC entry 4251 (class 2604 OID 16502)
-- Name: factura id; Type: DEFAULT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.factura ALTER COLUMN id SET DEFAULT nextval('sidq.factura_id_seq'::regclass);


--
-- TOC entry 4250 (class 2604 OID 16495)
-- Name: forma_pago id; Type: DEFAULT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.forma_pago ALTER COLUMN id SET DEFAULT nextval('sidq.forma_pago_id_seq'::regclass);


--
-- TOC entry 4246 (class 2604 OID 16467)
-- Name: marca id; Type: DEFAULT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.marca ALTER COLUMN id SET DEFAULT nextval('sidq.marca_id_seq'::regclass);


--
-- TOC entry 4245 (class 2604 OID 16460)
-- Name: producto id; Type: DEFAULT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.producto ALTER COLUMN id SET DEFAULT nextval('sidq.producto_id_seq'::regclass);


--
-- TOC entry 4247 (class 2604 OID 16474)
-- Name: proveedor id; Type: DEFAULT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.proveedor ALTER COLUMN id SET DEFAULT nextval('sidq.proveedor_id_seq'::regclass);


--
-- TOC entry 4241 (class 2604 OID 16432)
-- Name: tipo_usuario id; Type: DEFAULT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.tipo_usuario ALTER COLUMN id SET DEFAULT nextval('sidq.tipo_usuario_id_seq'::regclass);


--
-- TOC entry 4242 (class 2604 OID 16439)
-- Name: usuario id; Type: DEFAULT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.usuario ALTER COLUMN id SET DEFAULT nextval('sidq.usuario_id_seq'::regclass);


--
-- TOC entry 4451 (class 0 OID 16443)
-- Dependencies: 216
-- Data for Name: caja; Type: TABLE DATA; Schema: sidq; Owner: postgres
--

INSERT INTO sidq.caja VALUES (1, 10001, 1);
INSERT INTO sidq.caja VALUES (5, 98, 1);
INSERT INTO sidq.caja VALUES (6, 90, 1);
INSERT INTO sidq.caja VALUES (2, 1004, 1);
INSERT INTO sidq.caja VALUES (3, 3500, 1);
INSERT INTO sidq.caja VALUES (4, 36, 1);
INSERT INTO sidq.caja VALUES (7, 9001, 0);
INSERT INTO sidq.caja VALUES (8, 9002, 1);
INSERT INTO sidq.caja VALUES (9, 14022, 0);


--
-- TOC entry 4453 (class 0 OID 16450)
-- Dependencies: 218
-- Data for Name: categoria; Type: TABLE DATA; Schema: sidq; Owner: postgres
--

INSERT INTO sidq.categoria VALUES (2, 'Oficina', 'Categoría de artículos de oficina.');
INSERT INTO sidq.categoria VALUES (4, 'Papelería', 'Categoría de papelería.');
INSERT INTO sidq.categoria VALUES (3, 'Mochilas Infantiles', 'Categoría de Mochilas para niños y niñas.');
INSERT INTO sidq.categoria VALUES (5, 'Muebles de Madera', 'Muebles de oficina');
INSERT INTO sidq.categoria VALUES (1, 'Categoria-A', 'Categoría de prueba.');


--
-- TOC entry 4469 (class 0 OID 16506)
-- Dependencies: 234
-- Data for Name: cliente; Type: TABLE DATA; Schema: sidq; Owner: postgres
--



--
-- TOC entry 4461 (class 0 OID 16478)
-- Dependencies: 226
-- Data for Name: compra; Type: TABLE DATA; Schema: sidq; Owner: postgres
--



--
-- TOC entry 4463 (class 0 OID 16485)
-- Dependencies: 228
-- Data for Name: detalle_compra; Type: TABLE DATA; Schema: sidq; Owner: postgres
--



--
-- TOC entry 4471 (class 0 OID 16513)
-- Dependencies: 236
-- Data for Name: detalle_factura; Type: TABLE DATA; Schema: sidq; Owner: postgres
--



--
-- TOC entry 4467 (class 0 OID 16499)
-- Dependencies: 232
-- Data for Name: factura; Type: TABLE DATA; Schema: sidq; Owner: postgres
--



--
-- TOC entry 4465 (class 0 OID 16492)
-- Dependencies: 230
-- Data for Name: forma_pago; Type: TABLE DATA; Schema: sidq; Owner: postgres
--



--
-- TOC entry 4457 (class 0 OID 16464)
-- Dependencies: 222
-- Data for Name: marca; Type: TABLE DATA; Schema: sidq; Owner: postgres
--

INSERT INTO sidq.marca VALUES (1, 'Marca-A', 1, '2022-08-29 00:00:00', 'Registro de Prueba', 0);
INSERT INTO sidq.marca VALUES (2, 'Marca-B', 1, '2022-08-29 04:05:13.7127', 'Registro de Prueba Dos', 0);
INSERT INTO sidq.marca VALUES (3, 'MARCA-C', 0, '2022-09-17 07:00:29.715527', 'Prueba de Marca C', 3);


--
-- TOC entry 4455 (class 0 OID 16457)
-- Dependencies: 220
-- Data for Name: producto; Type: TABLE DATA; Schema: sidq; Owner: postgres
--

INSERT INTO sidq.producto VALUES (12, 'Producto UNO', 'Producto de prueba', 1, 200, 2.00, 2, 1, 1, NULL);


--
-- TOC entry 4459 (class 0 OID 16471)
-- Dependencies: 224
-- Data for Name: proveedor; Type: TABLE DATA; Schema: sidq; Owner: postgres
--

INSERT INTO sidq.proveedor VALUES (1, 'Proveedor Uno', 'Ciudad de Guatemala', '22332244', '44114411', 'mail@correo.com', 'https://www.demo.com', 'P-001', NULL, 1);
INSERT INTO sidq.proveedor VALUES (2, 'Proveedor 2', 'Ciudad de Guatemala', '22222222', '44444444', 'proveedor2@correo.com', 'www.proveedor2.com', 'P-002', NULL, 1);


--
-- TOC entry 4447 (class 0 OID 16429)
-- Dependencies: 212
-- Data for Name: tipo_usuario; Type: TABLE DATA; Schema: sidq; Owner: postgres
--



--
-- TOC entry 4449 (class 0 OID 16436)
-- Dependencies: 214
-- Data for Name: usuario; Type: TABLE DATA; Schema: sidq; Owner: postgres
--



--
-- TOC entry 4492 (class 0 OID 0)
-- Dependencies: 215
-- Name: caja_id_seq; Type: SEQUENCE SET; Schema: sidq; Owner: postgres
--

SELECT pg_catalog.setval('sidq.caja_id_seq', 9, true);


--
-- TOC entry 4493 (class 0 OID 0)
-- Dependencies: 217
-- Name: categoria_id_seq; Type: SEQUENCE SET; Schema: sidq; Owner: postgres
--

SELECT pg_catalog.setval('sidq.categoria_id_seq', 5, true);


--
-- TOC entry 4494 (class 0 OID 0)
-- Dependencies: 233
-- Name: cliente_id_seq; Type: SEQUENCE SET; Schema: sidq; Owner: postgres
--

SELECT pg_catalog.setval('sidq.cliente_id_seq', 1, false);


--
-- TOC entry 4495 (class 0 OID 0)
-- Dependencies: 225
-- Name: compra_id_seq; Type: SEQUENCE SET; Schema: sidq; Owner: postgres
--

SELECT pg_catalog.setval('sidq.compra_id_seq', 1, false);


--
-- TOC entry 4496 (class 0 OID 0)
-- Dependencies: 227
-- Name: detalle_compra_id_seq; Type: SEQUENCE SET; Schema: sidq; Owner: postgres
--

SELECT pg_catalog.setval('sidq.detalle_compra_id_seq', 1, false);


--
-- TOC entry 4497 (class 0 OID 0)
-- Dependencies: 235
-- Name: detalle_factura_id_seq; Type: SEQUENCE SET; Schema: sidq; Owner: postgres
--

SELECT pg_catalog.setval('sidq.detalle_factura_id_seq', 1, false);


--
-- TOC entry 4498 (class 0 OID 0)
-- Dependencies: 231
-- Name: factura_id_seq; Type: SEQUENCE SET; Schema: sidq; Owner: postgres
--

SELECT pg_catalog.setval('sidq.factura_id_seq', 1, false);


--
-- TOC entry 4499 (class 0 OID 0)
-- Dependencies: 229
-- Name: forma_pago_id_seq; Type: SEQUENCE SET; Schema: sidq; Owner: postgres
--

SELECT pg_catalog.setval('sidq.forma_pago_id_seq', 1, false);


--
-- TOC entry 4500 (class 0 OID 0)
-- Dependencies: 221
-- Name: marca_id_seq; Type: SEQUENCE SET; Schema: sidq; Owner: postgres
--

SELECT pg_catalog.setval('sidq.marca_id_seq', 3, true);


--
-- TOC entry 4501 (class 0 OID 0)
-- Dependencies: 219
-- Name: producto_id_seq; Type: SEQUENCE SET; Schema: sidq; Owner: postgres
--

SELECT pg_catalog.setval('sidq.producto_id_seq', 12, true);


--
-- TOC entry 4502 (class 0 OID 0)
-- Dependencies: 223
-- Name: proveedor_id_seq; Type: SEQUENCE SET; Schema: sidq; Owner: postgres
--

SELECT pg_catalog.setval('sidq.proveedor_id_seq', 2, true);


--
-- TOC entry 4503 (class 0 OID 0)
-- Dependencies: 211
-- Name: tipo_usuario_id_seq; Type: SEQUENCE SET; Schema: sidq; Owner: postgres
--

SELECT pg_catalog.setval('sidq.tipo_usuario_id_seq', 1, false);


--
-- TOC entry 4504 (class 0 OID 0)
-- Dependencies: 213
-- Name: usuario_id_seq; Type: SEQUENCE SET; Schema: sidq; Owner: postgres
--

SELECT pg_catalog.setval('sidq.usuario_id_seq', 1, false);


--
-- TOC entry 4262 (class 2606 OID 16448)
-- Name: caja pk_caja; Type: CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.caja
    ADD CONSTRAINT pk_caja PRIMARY KEY (id);


--
-- TOC entry 4265 (class 2606 OID 16455)
-- Name: categoria pk_categoria; Type: CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.categoria
    ADD CONSTRAINT pk_categoria PRIMARY KEY (id);


--
-- TOC entry 4289 (class 2606 OID 16511)
-- Name: cliente pk_cliente; Type: CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.cliente
    ADD CONSTRAINT pk_cliente PRIMARY KEY (id);


--
-- TOC entry 4277 (class 2606 OID 16483)
-- Name: compra pk_compra; Type: CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.compra
    ADD CONSTRAINT pk_compra PRIMARY KEY (id);


--
-- TOC entry 4280 (class 2606 OID 16490)
-- Name: detalle_compra pk_detalle_compra; Type: CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.detalle_compra
    ADD CONSTRAINT pk_detalle_compra PRIMARY KEY (id);


--
-- TOC entry 4292 (class 2606 OID 16518)
-- Name: detalle_factura pk_detalle_factura; Type: CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.detalle_factura
    ADD CONSTRAINT pk_detalle_factura PRIMARY KEY (id);


--
-- TOC entry 4286 (class 2606 OID 16504)
-- Name: factura pk_factura; Type: CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.factura
    ADD CONSTRAINT pk_factura PRIMARY KEY (id);


--
-- TOC entry 4283 (class 2606 OID 16497)
-- Name: forma_pago pk_forma_pago; Type: CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.forma_pago
    ADD CONSTRAINT pk_forma_pago PRIMARY KEY (id);


--
-- TOC entry 4271 (class 2606 OID 16469)
-- Name: marca pk_marca; Type: CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.marca
    ADD CONSTRAINT pk_marca PRIMARY KEY (id);


--
-- TOC entry 4267 (class 2606 OID 16462)
-- Name: producto pk_producto; Type: CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.producto
    ADD CONSTRAINT pk_producto PRIMARY KEY (id);


--
-- TOC entry 4273 (class 2606 OID 16476)
-- Name: proveedor pk_proveedor; Type: CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.proveedor
    ADD CONSTRAINT pk_proveedor PRIMARY KEY (id);


--
-- TOC entry 4255 (class 2606 OID 16434)
-- Name: tipo_usuario pk_tipo_usuario; Type: CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.tipo_usuario
    ADD CONSTRAINT pk_tipo_usuario PRIMARY KEY (id);


--
-- TOC entry 4258 (class 2606 OID 16441)
-- Name: usuario pk_usuario; Type: CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.usuario
    ADD CONSTRAINT pk_usuario PRIMARY KEY (id);


--
-- TOC entry 4260 (class 1259 OID 16521)
-- Name: caja_id; Type: INDEX; Schema: sidq; Owner: postgres
--

CREATE UNIQUE INDEX caja_id ON sidq.caja USING btree (id);


--
-- TOC entry 4263 (class 1259 OID 16522)
-- Name: categoria_id; Type: INDEX; Schema: sidq; Owner: postgres
--

CREATE UNIQUE INDEX categoria_id ON sidq.categoria USING btree (id);


--
-- TOC entry 4287 (class 1259 OID 16530)
-- Name: cliente_id; Type: INDEX; Schema: sidq; Owner: postgres
--

CREATE UNIQUE INDEX cliente_id ON sidq.cliente USING btree (id);


--
-- TOC entry 4275 (class 1259 OID 16526)
-- Name: compra_id; Type: INDEX; Schema: sidq; Owner: postgres
--

CREATE UNIQUE INDEX compra_id ON sidq.compra USING btree (id);


--
-- TOC entry 4278 (class 1259 OID 16527)
-- Name: detalle_compra_id; Type: INDEX; Schema: sidq; Owner: postgres
--

CREATE UNIQUE INDEX detalle_compra_id ON sidq.detalle_compra USING btree (id);


--
-- TOC entry 4290 (class 1259 OID 16531)
-- Name: detalle_factura_id; Type: INDEX; Schema: sidq; Owner: postgres
--

CREATE UNIQUE INDEX detalle_factura_id ON sidq.detalle_factura USING btree (id);


--
-- TOC entry 4284 (class 1259 OID 16529)
-- Name: factura_id; Type: INDEX; Schema: sidq; Owner: postgres
--

CREATE UNIQUE INDEX factura_id ON sidq.factura USING btree (id);


--
-- TOC entry 4281 (class 1259 OID 16528)
-- Name: forma_pago_id; Type: INDEX; Schema: sidq; Owner: postgres
--

CREATE UNIQUE INDEX forma_pago_id ON sidq.forma_pago USING btree (id);


--
-- TOC entry 4269 (class 1259 OID 16524)
-- Name: marca_id; Type: INDEX; Schema: sidq; Owner: postgres
--

CREATE UNIQUE INDEX marca_id ON sidq.marca USING btree (id);


--
-- TOC entry 4268 (class 1259 OID 16523)
-- Name: producto_id; Type: INDEX; Schema: sidq; Owner: postgres
--

CREATE UNIQUE INDEX producto_id ON sidq.producto USING btree (id);


--
-- TOC entry 4274 (class 1259 OID 16525)
-- Name: proveedor_id; Type: INDEX; Schema: sidq; Owner: postgres
--

CREATE UNIQUE INDEX proveedor_id ON sidq.proveedor USING btree (id);


--
-- TOC entry 4256 (class 1259 OID 16519)
-- Name: tipo_usuario_id; Type: INDEX; Schema: sidq; Owner: postgres
--

CREATE UNIQUE INDEX tipo_usuario_id ON sidq.tipo_usuario USING btree (id);


--
-- TOC entry 4259 (class 1259 OID 16520)
-- Name: usuario_id; Type: INDEX; Schema: sidq; Owner: postgres
--

CREATE UNIQUE INDEX usuario_id ON sidq.usuario USING btree (id);


--
-- TOC entry 4301 (class 2606 OID 16567)
-- Name: factura fk_caja; Type: FK CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.factura
    ADD CONSTRAINT fk_caja FOREIGN KEY (caja) REFERENCES sidq.caja(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 4294 (class 2606 OID 16537)
-- Name: producto fk_categoria; Type: FK CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.producto
    ADD CONSTRAINT fk_categoria FOREIGN KEY (categoria) REFERENCES sidq.categoria(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 4304 (class 2606 OID 16587)
-- Name: factura fk_cliente_factura; Type: FK CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.factura
    ADD CONSTRAINT fk_cliente_factura FOREIGN KEY (cliente) REFERENCES sidq.cliente(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 4299 (class 2606 OID 16552)
-- Name: detalle_compra fk_compra; Type: FK CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.detalle_compra
    ADD CONSTRAINT fk_compra FOREIGN KEY (compra) REFERENCES sidq.compra(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 4305 (class 2606 OID 16592)
-- Name: detalle_factura fk_factura; Type: FK CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.detalle_factura
    ADD CONSTRAINT fk_factura FOREIGN KEY (factura) REFERENCES sidq.factura(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 4297 (class 2606 OID 16562)
-- Name: compra fk_forma_pago; Type: FK CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.compra
    ADD CONSTRAINT fk_forma_pago FOREIGN KEY (forma_pago) REFERENCES sidq.forma_pago(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 4302 (class 2606 OID 16577)
-- Name: factura fk_forma_pago_factura; Type: FK CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.factura
    ADD CONSTRAINT fk_forma_pago_factura FOREIGN KEY (forma_pago) REFERENCES sidq.forma_pago(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 4295 (class 2606 OID 16542)
-- Name: producto fk_marca; Type: FK CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.producto
    ADD CONSTRAINT fk_marca FOREIGN KEY (marca) REFERENCES sidq.marca(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 4300 (class 2606 OID 16557)
-- Name: detalle_compra fk_producto_compra; Type: FK CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.detalle_compra
    ADD CONSTRAINT fk_producto_compra FOREIGN KEY (producto) REFERENCES sidq.producto(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 4306 (class 2606 OID 16597)
-- Name: detalle_factura fk_producto_factura; Type: FK CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.detalle_factura
    ADD CONSTRAINT fk_producto_factura FOREIGN KEY (producto) REFERENCES sidq.producto(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 4296 (class 2606 OID 16547)
-- Name: producto fk_proveedor; Type: FK CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.producto
    ADD CONSTRAINT fk_proveedor FOREIGN KEY (proveedor) REFERENCES sidq.proveedor(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 4293 (class 2606 OID 16532)
-- Name: usuario fk_tipo_usuario; Type: FK CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.usuario
    ADD CONSTRAINT fk_tipo_usuario FOREIGN KEY (tipo_usuario) REFERENCES sidq.tipo_usuario(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 4298 (class 2606 OID 16572)
-- Name: compra fk_usuario; Type: FK CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.compra
    ADD CONSTRAINT fk_usuario FOREIGN KEY (usuario) REFERENCES sidq.usuario(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 4303 (class 2606 OID 16582)
-- Name: factura fk_usuario_factura; Type: FK CONSTRAINT; Schema: sidq; Owner: postgres
--

ALTER TABLE ONLY sidq.factura
    ADD CONSTRAINT fk_usuario_factura FOREIGN KEY (usuario) REFERENCES sidq.usuario(id) ON UPDATE CASCADE ON DELETE SET NULL;


-- Completed on 2022-09-17 11:22:49

--
-- PostgreSQL database dump complete
--

